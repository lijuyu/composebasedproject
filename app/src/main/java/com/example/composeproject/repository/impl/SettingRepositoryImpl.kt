package com.example.composeproject.repository.impl

import com.example.composeproject.dao.SettingDao
import com.example.composeproject.entity.UploadEntity
import com.example.composeproject.repository.SettingRepository

class SettingRepositoryImpl(val dao: SettingDao): SettingRepository {

    override suspend fun upload(data: UploadEntity) {

    }

    override suspend fun getData(): UploadEntity {
        val notes = dao.getNote()
        val tags = dao.getTag()
        val crossRef = dao.getNoteTagCrossRef()
        val user = dao.getUser()
        return UploadEntity(
            notes = notes,
            tags = tags,
            tagWithNote = crossRef,
            user = user
        )
    }


}