package com.example.composeproject.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import kotlinx.coroutines.flow.Flow

interface NoteRepository {

    suspend fun getData(id: Long): NoteWithTag

    suspend fun updateTitle(note: NoteEntity)

    suspend fun updateNoteContent(note: NoteWithTag)

    suspend fun delete(name: String)

    suspend fun addNote(note: NoteWithTag)

    suspend fun addOrUpdateNoteFile(name: String,content: String): Boolean

    suspend fun addImg(name: String, drawable: Drawable): String

    suspend fun deleteImg(name: String)

    suspend fun readNote(name: String): String

    fun pathToFilePath(path: String):String
}