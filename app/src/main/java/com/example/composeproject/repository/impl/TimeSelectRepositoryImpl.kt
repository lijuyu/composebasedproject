package com.example.composeproject.repository.impl

import androidx.compose.runtime.mutableStateOf
import com.example.composeproject.entity.DayEntity
import com.example.composeproject.entity.MonthEntity
import com.example.composeproject.repository.TimeSelectRepository
import com.example.composeproject.util.getCurrentMonth
import com.example.composeproject.util.getDayIntCurrentMonth
import com.example.composeproject.util.monthInEnglish

class TimeSelectRepositoryImpl(): TimeSelectRepository {

    private var selectMonth  = mutableStateOf(MonthEntity())
    private var selectDay  = mutableStateOf(DayEntity())

    override fun getMonthList(): List<MonthEntity>{
        var startTime = 1
        val list = mutableListOf<MonthEntity>()
        val currentMonth = getCurrentMonth() - 1

        for(i in 0..11){
            val timeEntity = MonthEntity(month = startTime,monthInEnglish = monthInEnglish(startTime),days = getDayIntCurrentMonth(startTime))
            timeEntity.days = getDayIntCurrentMonth(startTime)
            if(i == currentMonth){
                timeEntity.isSelect = true
            }
            list.add(timeEntity)
            startTime++
        }
        return list
    }

    override fun observeMonthSelected(): MonthEntity = selectMonth.value

    override fun observeDaySelected(): DayEntity = selectDay.value

    override fun selectMonth(month: MonthEntity) {
        selectMonth.value = month
    }

    override fun selectDay(day: DayEntity) {
        selectDay.value = day
    }
}