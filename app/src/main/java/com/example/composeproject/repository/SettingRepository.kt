package com.example.composeproject.repository

import com.example.composeproject.entity.UploadEntity

interface SettingRepository {

    suspend fun upload(data: UploadEntity)

    suspend fun getData(): UploadEntity
}