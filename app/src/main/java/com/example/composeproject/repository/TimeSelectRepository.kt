package com.example.composeproject.repository

import com.example.composeproject.entity.DayEntity
import com.example.composeproject.entity.MonthEntity

interface TimeSelectRepository: BaseRepository {

    fun getMonthList(): List<MonthEntity>

    fun observeMonthSelected(): MonthEntity

    fun observeDaySelected(): DayEntity

    fun selectMonth(month: MonthEntity)

    fun selectDay(day: DayEntity)
}