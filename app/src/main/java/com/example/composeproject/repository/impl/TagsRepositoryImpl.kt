package com.example.composeproject.repository.impl

import com.example.composeproject.dao.TagDao
import com.example.composeproject.entity.BasicTagEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import com.example.composeproject.repository.TagsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList

class TagsRepositoryImpl(private val dao: TagDao): TagsRepository {

    override suspend fun addTagWithNoteId(noteId: Long) {
        //todo addTag
    }

    override suspend fun getAllTag(): Flow<List<TagWithNote>> {
        return dao.getAllTag()
    }

    override suspend fun getChildTags(parenId: Long): Flow<List<TagWithNote>> {
        return dao.getChildTags(parenId = parenId)
    }

    override suspend fun getTagsByNoteId(noteId: Long): Flow<NoteWithTag> {
        return dao.getTagsByNoteId(noteId = noteId)
    }

    override suspend fun getNotesByTagId(tagId: Long): Flow<TagWithNote> {
        return dao.getNotesByTagId(tagId = tagId)
    }

    override suspend fun addChildTag(tag: TagEntity) {
        dao.addChildTag(tag = tag)
    }


}