package com.example.composeproject.repository.impl

import com.example.composeproject.dao.HomeDao
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.repository.HomeRepository
import kotlinx.coroutines.flow.Flow

class HomeRepositoryImpl(private val homeDao: HomeDao): HomeRepository {

    override suspend fun getNote(): Flow<List<NoteWithTag>> {
        return homeDao.getNoteList()
    }

    override suspend fun getTagsNum(): Int {
        return homeDao.getTagsNum()
    }

    override suspend fun getNoteNum(): Int {
        return homeDao.getNoteNum()
    }

    override suspend fun downLoadData() {

    }

    override suspend fun deleteNoteList(noteList: List<NoteEntity>) {
        homeDao.deleteNotes(noteList = noteList)
    }

    override suspend fun deleteNote(note: NoteEntity) {
        homeDao.deleteNote(note = note)
    }

    override suspend fun uploadNote(note: NoteEntity) {

    }

    override suspend fun uploadNoteList(noteList: List<NoteEntity>) {

    }

}