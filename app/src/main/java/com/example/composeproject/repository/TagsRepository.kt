package com.example.composeproject.repository

import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import kotlinx.coroutines.flow.Flow

interface TagsRepository: BaseRepository {

    suspend fun addTagWithNoteId(noteId: Long)

    suspend fun getAllTag(): Flow<List<TagWithNote>>

    suspend fun getChildTags(parenId: Long): Flow<List<TagWithNote>>

    suspend fun getTagsByNoteId(noteId: Long): Flow<NoteWithTag>

    suspend fun getNotesByTagId(tagId: Long): Flow<TagWithNote>

    suspend fun addChildTag(tag: TagEntity)

}