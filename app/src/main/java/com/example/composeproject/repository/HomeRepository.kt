package com.example.composeproject.repository

import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import kotlinx.coroutines.flow.Flow

/**
 * 首页笔记数据列表获取
 */
interface HomeRepository : BaseRepository{

    //获取笔记
    suspend fun getNote(): Flow<List<NoteWithTag>>

    suspend fun getTagsNum(): Int

    suspend fun getNoteNum() :Int

    suspend fun downLoadData()

    suspend fun deleteNoteList(noteList: List<NoteEntity>)

    suspend fun deleteNote(note: NoteEntity)

    suspend fun uploadNote(note: NoteEntity)

    suspend fun uploadNoteList(noteList: List<NoteEntity>)

}