package com.example.composeproject.repository.impl

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.composeproject.dao.NoteDao
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteTagCrossRef
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.repository.NoteRepository
import com.example.composeproject.util.*
import kotlinx.coroutines.flow.Flow
import java.io.File


class NoteRepositoryImpl(val context: Context,val dao: NoteDao): NoteRepository {

    override suspend fun getData(id: Long): NoteWithTag {
        return dao.getData(id)
    }

    override suspend fun updateTitle(note: NoteEntity) {
        dao.updateTitle(note = note)
    }

    override suspend fun updateNoteContent(note: NoteWithTag) {
        dao.updateContent(note = note.note)
        if(note.tagList.isNotEmpty()){
            val tagIds = dao.addTags(tags = note.tagList)
            val list = mutableListOf<NoteTagCrossRef>()
            tagIds.filter { it != -1L }.forEach{
                list.add(NoteTagCrossRef(tagId = it,noteId = note.note.noteId))
            }
            dao.addNoteWithTags(list)
        }
    }

    override suspend fun delete( name: String) {
        deleteNoteFile(context = context,name = name)
    }

    override suspend fun addNote(note: NoteWithTag) {
        val id = dao.addNote(note = note.note)
        log("添加的id$id")
        if(note.tagList.isNotEmpty()){
            val tagIds = dao.addTags(tags = note.tagList)
            val list = mutableListOf<NoteTagCrossRef>()
            tagIds.filter { it != -1L }.forEach {
                list.add(NoteTagCrossRef(tagId = it,noteId = id))
            }
            dao.addNoteWithTags(list)
        }
    }

    override suspend fun addOrUpdateNoteFile(name: String,content: String): Boolean {
        return addOrUpdateNoteFile(context = context,name = name,content = content)
    }

    override suspend fun addImg(name: String, drawable: Drawable): String{
        return drawableToFile(context = context,drawable = drawable,name = name)
    }

    override suspend fun deleteImg(name: String) {
        deleteImage(context = context,name = name)
    }

    override suspend fun readNote(name: String): String {
        return readNoteFile(context = context,name = name)
    }

    override fun pathToFilePath(path: String): String {
        val file = File(context.getExternalFilesDir(null),"$path.png")
        return file.path
    }
}