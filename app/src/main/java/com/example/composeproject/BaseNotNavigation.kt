package com.example.composeproject

import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.util.log


object NoteNavDestinations{
    const val HOME_ROUTE = "home"
    const val TIME_SELECT_ROUTE = "time_select"
    const val TAGS_ROUTE = "tags"
    const val NOTE_DETAIL_ROUTE = "note_detail"
    const val SETTING_ROUTE = "setting"
}

class BaseNotNavigationActions(private val navController: NavHostController) {
    val navigateHome: (Long?) -> Unit = {
        navController.navigate("${NoteNavDestinations.HOME_ROUTE }?time=${it?:0}"){
            popUpTo(navController.graph.findStartDestination().id){
                saveState = it == null
            }
            launchSingleTop = true
            restoreState = it == null
        }
    }

    val navigateTimeSelect: () -> Unit = {
        navController.navigate(NoteNavDestinations.TIME_SELECT_ROUTE){
            popUpTo(navController.graph.findStartDestination().id){
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    val navigateTags: () -> Unit = {
        navController.navigate(NoteNavDestinations.TAGS_ROUTE){
            popUpTo(navController.graph.findStartDestination().id){
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    val navigateNote : ((NoteEntity?) -> Unit) = {
        navController.navigate("${NoteNavDestinations.NOTE_DETAIL_ROUTE}?noteId=${it?.noteId?:0}"){
            popUpTo(navController.graph.findStartDestination().id){
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    val navigateSetting:() -> Unit = {
        navController.navigate(NoteNavDestinations.SETTING_ROUTE){
            popUpTo(navController.graph.findStartDestination().id){
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }
}