package com.example.composeproject

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi

@ExperimentalPagerApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun NoteApp(
    navigate: String? = null,
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean = false
) {
    val navController = rememberAnimatedNavController()
    val navigationActions = remember(navController) {
        BaseNotNavigationActions(navController)
    }
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute =
        navBackStackEntry?.destination?.route ?: NoteNavDestinations.HOME_ROUTE
    Row(Modifier.fillMaxSize()) {
        NoteNavGraph(
            navController,
            currentRoute,
            navigateTimeSelect = navigationActions.navigateTimeSelect,
            naviGateHome = navigationActions.navigateHome,
            navigateTag = navigationActions.navigateTags,
            navigateNote = navigationActions.navigateNote,
            navigateSetting = navigationActions.navigateSetting,
            startDestination = navigate,
            themeModeChange = themeModeChange,
            isDark = isDark
        )
    }
}