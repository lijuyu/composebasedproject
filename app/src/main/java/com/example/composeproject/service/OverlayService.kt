package com.example.composeproject.service

import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.WindowManager
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Recomposer
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.AndroidUiDispatcher
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.compositionContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewTreeLifecycleOwner
import androidx.lifecycle.ViewTreeViewModelStoreOwner
import androidx.savedstate.ViewTreeSavedStateRegistryOwner
import com.example.composeproject.NoteNavDestinations
import com.example.composeproject.lifecycleowner.MyLifecycleOwner
import com.example.composeproject.ui.home.MainActivity
import com.example.composeproject.ui.overlay.OverlayLisenter
import com.example.composeproject.ui.overlay.OverlayWindow
import com.example.composeproject.ui.theme.PrimaryColor
import com.example.composeproject.util.log
import com.example.composeproject.util.screenWidth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class OverlayService : Service(), OverlayLisenter {

    private val windowManager by lazy {
        getSystemService(WINDOW_SERVICE) as WindowManager
    }
    private val composeView by lazy {
        ComposeView(this).apply {
            setContent {
                OverlayWindow(
                    onMoved = ::move,
                    animate = ::adsorptionAnimate,
                    open = ::startMain
                )
            }
        }
    }
    private val layoutFlag: Int by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_PHONE
        }
    }
    private val params by lazy {
        WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            layoutFlag,
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            PixelFormat.TRANSLUCENT
        )
    }

    private val MyBinder: MyServiceBinder by lazy { MyServiceBinder() }

    override fun onCreate() {
        super.onCreate()
//        showOverlay()
//        calculateSizeAndPosition(params = params, 300, 300)

        val coroutineContext = AndroidUiDispatcher.CurrentThread
        val runRecomposeScope = CoroutineScope(coroutineContext)
        val recomposer = Recomposer(coroutineContext)
        composeView.compositionContext = recomposer
        runRecomposeScope.launch {
            recomposer.runRecomposeAndApplyChanges()
            recomposer.awaitIdle()
        }
        val viewModelStore = ViewModelStore()
        val lifecycleOwner = MyLifecycleOwner()
        lifecycleOwner.performRestore(null)
        lifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        ViewTreeLifecycleOwner.set(composeView, lifecycleOwner)
        ViewTreeViewModelStoreOwner.set(composeView) { viewModelStore }
        ViewTreeSavedStateRegistryOwner.set(composeView, lifecycleOwner)
    }

    private fun showOverlay() {
        windowManager.addView(composeView, params)
    }

    private fun hideOverLay() {
        windowManager.removeView(composeView)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return MyBinder
    }

    override fun open() {
        showOverlay()
    }

    override fun close() {
        hideOverLay()
    }

    private fun move(x: Int,y: Int){
        params.y = y
        params.x = x
        windowManager.updateViewLayout(composeView,params)
    }

    private fun adsorptionAnimate(){
        if(params.x >= screenWidth()/2 - 80){
            params.x = screenWidth()/2
        }else if( params.x <= -screenWidth()/2 + 80){
            params.x = -screenWidth()/2
        }
        windowManager.updateViewLayout(composeView,params)
    }

    inner class MyServiceBinder : Binder() {
        fun getInstance(): OverlayService {
            return this@OverlayService
        }
    }

    private fun startMain(){
        MainActivity.launch(this,"${NoteNavDestinations.NOTE_DETAIL_ROUTE}?noteId={noteId}",isNewTask = true)
    }

}