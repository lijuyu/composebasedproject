package com.example.composeproject.entity

import android.graphics.Bitmap

data class NoteTextEntity(
    var fontSize: Int = 25,
    var textColor: String = "#000000",
    var isBold: Boolean = false,
    var isItalic: Boolean = false,
    var path: String = "",
    var text: String = "",
    var bitmap: Bitmap? = null,
    var type: ContentType = ContentType.Text
)

enum class ContentType{
    Image,Text
}