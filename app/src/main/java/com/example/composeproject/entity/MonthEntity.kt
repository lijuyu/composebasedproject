package com.example.composeproject.entity

import com.example.composeproject.util.monthInEnglish

data class MonthEntity(
    val time: Long = 0,
    val month: Int = 0,
    var days: List<DayEntity> = listOf(),
    val year: Int = 0,
    var isSelect: Boolean = false,
    var monthInEnglish: String = ""
){
    init {
        monthInEnglish = monthInEnglish(month)
    }
}