package com.example.composeproject.entity

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UploadEntity(
    val notes: List<NoteEntity>,
    val tags: List<TagEntity>,
    val user:UserEntity,
    val tagWithNote: List<NoteTagCrossRef>
)

@JsonClass(generateAdapter = true)
data class DownloadEntity(
    val notes: List<NoteEntity>,
    val tags: List<TagEntity>,
    val user:UserEntity,
    val tagWithNote: List<NoteTagCrossRef>
)