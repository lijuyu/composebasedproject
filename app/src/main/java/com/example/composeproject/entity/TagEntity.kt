package com.example.composeproject.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.squareup.moshi.JsonClass
import org.jetbrains.annotations.NotNull

@JsonClass(generateAdapter = true)
@Entity(tableName = "tag_table",foreignKeys = [ForeignKey(
    entity = TagEntity::class,
    childColumns = ["parentId"],
    parentColumns = ["tagId"],
    onDelete = CASCADE,
    onUpdate = CASCADE
)])
data class TagEntity(

    @PrimaryKey(autoGenerate = true)
    val tagId: Long = 0,
    @NotNull
    val name: String = "",
    @ColumnInfo(index = true)
    val parentId : Long? = null
)