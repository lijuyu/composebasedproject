package com.example.composeproject.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(
    tableName = "note_tag_table",
    primaryKeys = ["tagId", "noteId"],
    foreignKeys = [ForeignKey(
        entity = TagEntity::class,
        childColumns = ["tagId"],
        parentColumns = ["tagId"],
        onUpdate = CASCADE,
        onDelete = CASCADE
    ), ForeignKey(
        entity = NoteEntity::class,
        childColumns = ["noteId"],
        parentColumns = ["noteId"],
        onUpdate = CASCADE,
        onDelete = CASCADE
    )], indices = [
        Index(
            name = "noteId",
            unique = false,
            value = ["noteId"]
        )
    ]
)
data class NoteTagCrossRef(
    @ColumnInfo(name = "tagId", index = true)
    val tagId: Long,
    @ColumnInfo(name = "noteId", index = true)
    val noteId: Long
)