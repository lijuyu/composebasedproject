package com.example.composeproject.entity

data class TextWithSize(
    var text: String = "",
    var textSize: Int = 0,
)