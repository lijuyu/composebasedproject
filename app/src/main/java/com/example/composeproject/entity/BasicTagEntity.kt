package com.example.composeproject.entity

data class BasicTagEntity(
    val id: Int = 0,
    val parent: BasicTagEntity? = null,
    var children: List<TagWithNote> = listOf(),
    val topTag: TagWithNote,
    var notesList: List<NoteEntity> = emptyList()
)