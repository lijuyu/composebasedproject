package com.example.composeproject.entity

data class DayEntity(
    var month: Int = 0,
    var day: Int = 0,
    var isSelect: Boolean = false
)