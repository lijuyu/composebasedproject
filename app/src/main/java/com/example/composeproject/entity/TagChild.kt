package com.example.composeproject.entity

import androidx.room.Embedded
import androidx.room.Relation

data class TagChild(
    @Embedded val tag: TagEntity,
    @Relation(
        parentColumn = "tagId",
        entityColumn = "tagId"
    )
    val tagList : List<TagEntity>
)