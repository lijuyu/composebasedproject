package com.example.composeproject.entity

import androidx.room.Embedded
import androidx.room.Relation

data class UserAndNote (
    @Embedded val user: UserEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val noteList: List<NoteEntity>
)