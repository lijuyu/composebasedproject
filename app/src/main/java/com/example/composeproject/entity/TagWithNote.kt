package com.example.composeproject.entity

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class TagWithNote (
    @Embedded
    val tag: TagEntity,
    @Relation(
        parentColumn = "tagId",
        entityColumn = "noteId",
        associateBy = Junction(NoteTagCrossRef::class)
    )
    val noteList: List<NoteEntity>
)
