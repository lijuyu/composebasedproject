package com.example.composeproject.entity

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.squareup.moshi.JsonClass
import org.jetbrains.annotations.NotNull
import java.util.*

@JsonClass(generateAdapter = true)
@Entity(tableName = "note_table",foreignKeys = [ForeignKey(
    entity = UserEntity::class,
    childColumns = ["userId"],
    parentColumns = ["id"],
    onDelete = CASCADE,
    onUpdate = CASCADE
)],indices = [Index(name = "userId",unique = false,value = ["userId"]),Index(name = "index_note_table_userId",unique = false,value = ["userId"])])
data class NoteEntity (

    @PrimaryKey(autoGenerate = true)
    val noteId: Long = 0,
    @NotNull
    var title: String = "",
    @NotNull
    var content: String = "",
    @NotNull
    var time: Date = Date(),
    var userId: Long = 0,
    @NotNull
    var path: String = ""
)