package com.example.composeproject.entity

import androidx.compose.ui.graphics.Color
import com.example.composeproject.ui.theme.PrimaryColor

data class ColorEntity(
    val color: Color = PrimaryColor,
    val isLight: Boolean = false,
)

data class FontColor(
    val value: String = "",
    val color: Color = Color.Black
)