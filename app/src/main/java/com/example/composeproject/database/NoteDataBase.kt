package com.example.composeproject.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.composeproject.dao.HomeDao
import com.example.composeproject.dao.NoteDao
import com.example.composeproject.dao.TagDao
import com.example.composeproject.dao.UserDao
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteTagCrossRef
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.UserEntity
import com.example.composeproject.util.Converters

@Database(entities = [UserEntity::class,TagEntity::class,NoteEntity::class,NoteTagCrossRef::class],version = 1,exportSchema = false)
@TypeConverters(Converters::class)
abstract class NoteDataBase: RoomDatabase() {

    abstract fun homeDao(): HomeDao
    abstract fun tagDao(): TagDao
    abstract fun noteDao() : NoteDao
    abstract fun userDao() :UserDao

    companion object{
        private const val MY_DB_NAME = "note_db"
        @Volatile
        private var database: NoteDataBase? = null

        fun getInstance(context: Context): NoteDataBase{
            return database ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context,
                    NoteDataBase::class.java,
                    MY_DB_NAME)
                    .createFromAsset("notedb.db")
                    .build()
                database = instance
                instance
            }
        }
    }
}