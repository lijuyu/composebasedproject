package com.example.composeproject

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.repository.impl.HomeRepositoryImpl
import com.example.composeproject.repository.impl.NoteRepositoryImpl
import com.example.composeproject.repository.impl.TagsRepositoryImpl
import com.example.composeproject.repository.impl.TimeSelectRepositoryImpl
import com.example.composeproject.ui.base.NoteApplication
import com.example.composeproject.ui.home.HomeRoute
import com.example.composeproject.ui.home.viewmodel.HomeViewModel
import com.example.composeproject.ui.notedetail.NoteDetailViewModel
import com.example.composeproject.ui.notedetail.NoteRoute
import com.example.composeproject.ui.setting.SettingRoute
import com.example.composeproject.ui.tags.TagsRoute
import com.example.composeproject.ui.tags.TagsViewModel
import com.example.composeproject.ui.timeselect.TimeSelectRoute
import com.example.composeproject.ui.timeselect.TimeSelectViewModel
import com.example.composeproject.util.OVERLAY_VALUE
import com.example.composeproject.util.SETTING_SHARED_PREF
import com.example.composeproject.util.log
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi


@ExperimentalPagerApi
@ExperimentalFoundationApi
@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun NoteNavGraph(
    navController: NavHostController,
    currentRoute: String,
    startDestination: String?,
    navigateTimeSelect: () -> Unit,
    naviGateHome: (Long?) -> Unit,
    navigateTag: () -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
    navigateSetting: () -> Unit,
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean
){
    val context = LocalContext.current
    val application = remember{ context.applicationContext as NoteApplication}
    val sharedPref = application.getSharedPrefEditInstance()

    AnimatedNavHost(
        navController = navController,
        startDestination = startDestination?:"${NoteNavDestinations.HOME_ROUTE}?time={time}" ,
    ){
        composable(
            "${NoteNavDestinations.HOME_ROUTE}?time={time}",
            arguments = listOf(navArgument("time"){
                type = NavType.LongType
                defaultValue = 0
            }),
            enterTransition = {
                when(this.initialState.destination.route){
                    NoteNavDestinations.TIME_SELECT_ROUTE ->{
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Up,
                            tween(500,easing= LinearOutSlowInEasing )
                        )
                    }
                    NoteNavDestinations.TAGS_ROUTE ->{
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            tween(500,150,easing= LinearOutSlowInEasing )
                        )
                    }
                    NoteNavDestinations.SETTING_ROUTE ->{
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            tween(500,150,easing= LinearOutSlowInEasing )
                        )
                    }
                    else -> null
                }
            },
            exitTransition = {
                when(this.targetState.destination.route){
                    NoteNavDestinations.TIME_SELECT_ROUTE ->{
                        fadeOut(tween(200))
                    }
                    NoteNavDestinations.TAGS_ROUTE ->{
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            tween(500,easing= LinearOutSlowInEasing)
                        )
                    }
                    NoteNavDestinations.SETTING_ROUTE ->{
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            tween(500,easing= LinearOutSlowInEasing)
                        )
                    }
                    else -> null
                }
            }
        ){ backEntry ->
            val repository = HomeRepositoryImpl(application.dataBase.homeDao())
            val homeViewModel: HomeViewModel = viewModel(factory = HomeViewModel.provideFactory(repository))
            HomeRoute(
                viewModel = homeViewModel,
                time = backEntry.arguments?.getLong("time")?:0,
                navigateTimeSelect = navigateTimeSelect,
                navigateTags = navigateTag,
                navigateNoteDetail = navigateNote,
                navigateSetting = navigateSetting
            )
        }

        composable(
            NoteNavDestinations.TIME_SELECT_ROUTE,
            enterTransition = {
                slideIntoContainer(
                    AnimatedContentScope.SlideDirection.Down,
                    animationSpec = spring(
                        stiffness = Spring.StiffnessLow,
                        dampingRatio = 0.65f
                    )
                )
            }
        ){
            val repository = TimeSelectRepositoryImpl()
            val timeSelectViewModel: TimeSelectViewModel = viewModel(factory = TimeSelectViewModel.provideFactory(repository))
            TimeSelectRoute(viewModel = timeSelectViewModel,navigateHome = naviGateHome)
        }

        composable(
            NoteNavDestinations.TAGS_ROUTE,
            enterTransition = {
                slideIntoContainer(
                    AnimatedContentScope.SlideDirection.Left,
                    tween(500,150,easing= LinearOutSlowInEasing )
                )
            },
            exitTransition = {
                slideOutOfContainer(
                    AnimatedContentScope.SlideDirection.Right,
                    tween(500,easing= LinearOutSlowInEasing)
                )
            }
        ){
            val repository = TagsRepositoryImpl(application.dataBase.tagDao())
            val viewModel: TagsViewModel = viewModel(factory = TagsViewModel.provideFactory(repository))
            TagsRoute(
                viewModel,
                navigateHome = {naviGateHome(null)},
                navigateNote = navigateNote,
                selectTag = viewModel::selectTag,
                backParent = viewModel::backParent,
                addTag = viewModel::addChildTag,
                changeShowAddTag = viewModel::changeShowAddTag
            )
        }
        composable(
            "${NoteNavDestinations.NOTE_DETAIL_ROUTE}?noteId={noteId}",
            arguments = listOf(navArgument("noteId"){
                type = NavType.LongType
                defaultValue = 0
            })
        ){bacStackEntry ->
            val repository = NoteRepositoryImpl(application.applicationContext,application.dataBase.noteDao())
            val viewModel: NoteDetailViewModel = viewModel(factory = NoteDetailViewModel.provideFactory(repository,id = bacStackEntry.arguments?.getLong("noteId")?:0))
            NoteRoute(viewModel = viewModel,navigateHome = {naviGateHome(null)})
        }
        composable(
            NoteNavDestinations.SETTING_ROUTE,
            enterTransition = {
                slideIntoContainer(
                    AnimatedContentScope.SlideDirection.Left,
                    tween(500,150,easing= LinearOutSlowInEasing )
                )
            },
            exitTransition = {
                slideOutOfContainer(
                    AnimatedContentScope.SlideDirection.Right,
                    tween(500,easing= LinearOutSlowInEasing)
                )
            }
        ){
            SettingRoute(
                isShowOverlay = sharedPref.getBoolean(OVERLAY_VALUE,false),
                navigateHome = {naviGateHome(null)},
                onOverlayChange = {
                    if(it){
                        application.overlayLisenter?.open()
                    }else{
                        application.overlayLisenter?.close()
                    }
                    with(sharedPref.edit()){
                        putBoolean(OVERLAY_VALUE,it)
                        apply()
                    }
                },
                themeModeChange = themeModeChange,
                isDark = isDark
            )
        }
    }
}