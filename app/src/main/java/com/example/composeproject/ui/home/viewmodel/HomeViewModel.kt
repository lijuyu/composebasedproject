package com.example.composeproject.ui.home.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.MonthEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.repository.HomeRepository
import com.example.composeproject.util.log
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

sealed interface HomeUiState{

    val allTagsNum: Int
    val allNoteNum: Int
    val showUserInfo: Boolean

    data class NoNotes(
        val error: String = "",
        override val allTagsNum: Int = 0,
        override val allNoteNum: Int = 0,
        override val showUserInfo: Boolean = false,
        ) : HomeUiState

    data class HasNotes(
        val noteList: List<NoteWithTag> = emptyList(),
        val selectNote: NoteEntity,
        val isNoteOpen: Boolean = false,
        val onDelete: Boolean = false,
        val deleteList: List<NoteEntity> = emptyList(),
        override val allTagsNum: Int = 0,
        override val allNoteNum: Int = 0,
        override val showUserInfo: Boolean = false,
        ) : HomeUiState

}

private data class HomeViewModelState(
    val isNoteOpen: Boolean = false,
    val selectNote: NoteEntity = NoteEntity(),
    val noteList: List<NoteWithTag> = emptyList(),
    val allTagsNum: Int= 0,
    val allNoteNum: Int= 0,
    val onDelete: Boolean = false,
    val deleteList: List<NoteEntity> = emptyList(),
    val showUserInfo: Boolean = false
) {
    fun topUiState(): HomeUiState =
        if (noteList.isEmpty()) {
            HomeUiState.NoNotes(
                allNoteNum = allNoteNum,
                allTagsNum = allTagsNum,
                showUserInfo = showUserInfo
            )
        } else {
            HomeUiState.HasNotes(
                noteList = noteList,
                selectNote = selectNote,
                isNoteOpen = isNoteOpen,
                allNoteNum = allNoteNum,
                allTagsNum = allTagsNum,
                showUserInfo = showUserInfo,
                deleteList = deleteList,
                onDelete = onDelete
            )
        }
}

class HomeViewModel(private val repository: HomeRepository) : ViewModel() {

    init {
        getNotes()
    }

    private val viewModeState = MutableStateFlow(HomeViewModelState())
    val uiState =
        viewModeState.map { it.topUiState() }
            .stateIn(
                viewModelScope,
                SharingStarted.Eagerly,
                viewModeState.value.topUiState()
            )

    fun getNotes() {
        viewModelScope.launch {
            val flow = repository.getNote()
            flow.collect {notes ->
                viewModeState.update {
                    it.copy(noteList = notes)
                }
            }
        }
    }

    fun getTagAndNoteNum(){
        viewModelScope.launch {
            val tagNum = repository.getTagsNum()
            val noteNum = repository.getNoteNum()
            viewModeState.update {
                it.copy(
                    allTagsNum = tagNum,
                    allNoteNum = noteNum
                )
            }
        }
    }

    fun userInfoShowOnChange(isShow: Boolean){
        viewModeState.update {
            it.copy(
                showUserInfo = isShow
            )
        }
    }

    fun deleteNote(note: NoteEntity? = null){
        viewModelScope.launch {
            if(note == null){
                if(viewModeState.value.deleteList.isNotEmpty()){
                    repository.deleteNoteList(viewModeState.value.deleteList)
                }
                viewModeState.update {
                    it.copy(
                        onDelete = false
                    )
                }
            }else{
                repository.deleteNote(note = note)
            }
        }
    }

    fun changeDeleteState(){
        viewModeState.update {
            it.copy(onDelete = !it.onDelete)
        }
    }

    fun changeNoteToDeleteList(v: Boolean,note: NoteEntity){
        viewModeState.update {
            var list = mutableListOf<NoteEntity>()
            list.addAll(it.deleteList)
            if(v){
                list.add(note)
            }else{
                list.remove(note)
            }
            it.copy(
                deleteList = list
            )
        }
    }

    companion object {
        fun provideFactory(
            postsRepository: HomeRepository,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return HomeViewModel(postsRepository) as T
            }
        }
    }
}

