package com.example.composeproject.ui.timeselect

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue

@ExperimentalFoundationApi
@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun TimeSelectRoute(
    viewModel: TimeSelectViewModel,
    navigateHome: (Long?) -> Unit
){
    val uiState by viewModel.uiState.collectAsState()
    TimeSelectScreen(
        uiState = uiState,
        selectedMonth = viewModel::selectMonth,
        selectedDay = viewModel::selectDay,
        navigateHome =  navigateHome
    )
}