package com.example.composeproject.ui.component

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.composeproject.R
import com.example.composeproject.ui.theme.PrimaryColor_20
import com.example.composeproject.ui.theme.PrimaryColor_35

@Composable
fun HomeDeleteButton(
    v: Boolean,
    changeDeleteState: () -> Unit,
    clickAction: () -> Unit
){
    AnimatedVisibility(
        visible = v,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Box(modifier = Modifier
            .clip(CircleShape)
            .size(80.dp)
            .background(color = PrimaryColor_20)
            .padding(10.dp)
            .clip(CircleShape)
            .background(color = PrimaryColor_35),
            contentAlignment = Alignment.Center
        ){
            Icon(
                painter = painterResource(id = R.drawable.delete_home),
                contentDescription = null,
                modifier = Modifier
                    .size(30.dp)
                    .clickable {
                        clickAction()
                    }
            )
        }
        if(v){
            BackHandler {
                changeDeleteState()
            }
        }
    }
}