package com.example.composeproject.ui.timeselect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.composeproject.entity.DayEntity
import com.example.composeproject.entity.MonthEntity
import com.example.composeproject.repository.TimeSelectRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

data class TimeUiState(
    var list: List<MonthEntity> = emptyList(),
    var selectDay: DayEntity = DayEntity()
){
    fun isSelect(day: DayEntity): Boolean{
        return selectDay.month == day.month && selectDay.day == day.day
    }

}

class TimeSelectViewModel(private val repository: TimeSelectRepository): ViewModel() {

    private val _uiState = MutableStateFlow(TimeUiState())
    val uiState = _uiState.asStateFlow()

    init {
        getMonths()
    }

    private fun getMonths(){
        viewModelScope.launch {
            val list = repository.getMonthList()
            var selectTime = MonthEntity()
            list.forEach {
                if(it.isSelect){
                    selectTime = it
                }
            }
            var selectDay = DayEntity()
            selectTime.days.forEach {
                if(it.isSelect){
                    selectDay = it
                }
            }
            _uiState.update {
                it.copy(
                    list = list,
                    selectDay = selectDay
                )
            }
        }
    }

    fun selectMonth(month: MonthEntity){
        viewModelScope.launch {
            _uiState.update {
                it.copy(

                )
            }
        }
    }

    fun selectDay(time: DayEntity){
        viewModelScope.launch {
            _uiState.update {
                it.copy(
                    selectDay = time
                )
            }
        }
    }


    companion object {
        fun provideFactory(
            repository: TimeSelectRepository,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return TimeSelectViewModel(repository) as T
            }
        }
    }

}