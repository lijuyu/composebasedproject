package com.example.composeproject.ui.overlay

import android.content.Context
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.composeproject.R
import com.example.composeproject.util.ModeBgColor
import com.example.composeproject.util.ModeColor
import com.example.composeproject.util.click
import com.example.composeproject.util.screenWidth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

@Composable
fun OverlayWindow(
    onMoved: (Int,Int) -> Unit,
    animate: () -> Unit,
    open: () -> Unit
){
    var expand by remember { mutableStateOf(true) }
    val alpha by animateFloatAsState(targetValue =  if(expand) 0.5f else 1f)
    val context = LocalContext.current.applicationContext

    Box(
        modifier = Modifier
            .alpha(alpha)
            .wrapContentSize()
            .clip(RoundedCornerShape(4.dp)),
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            AnimatedVisibility(
                visible = !expand,
                enter = slideInVertically(
                    initialOffsetY = { fullHeight -> fullHeight },
                    animationSpec = tween(300)
                ),
                exit = slideOutVertically(
                    targetOffsetY = { fullHeight -> 0 },
                    animationSpec = tween(300)
                )
            ) {
                AddIcon(
                    open = open
                )
            }
            Spacer(modifier = Modifier.height(20.dp))
            LauncherIcon(
                context = context,
                expand = expand,
                onMoved = onMoved,
                onExpended = {
                    expand = it
                },
                animate = animate
            )
        }
    }

}


@Composable
fun LauncherIcon(
    context: Context,
    expand: Boolean,
    onMoved: (Int,Int) -> Unit,
    onExpended: (Boolean) -> Unit,
    animate: () -> Unit
){
    var offsetX by remember{ mutableStateOf(0f)}
    var offsetY by remember{ mutableStateOf(0f)}
    val scope = rememberCoroutineScope()
    var canClick = remember { true }
    Image(
        painter = painterResource(id = R.drawable.my_launcher),
        contentDescription = "悬浮窗",
        modifier = Modifier
            .size(40.dp)
            .clip(CircleShape)
            .pointerInput(Unit) {
                detectDragGestures(
                    onDragEnd = {
                        canClick = when {
                            offsetX >= context.screenWidth() / 2 - 80 -> {
                                false
                            }
                            offsetX <= -context.screenWidth() / 2 + 80 -> {
                                false
                            }
                            else -> {
                                true
                            }
                        }
                        scope.launch {
                            delay(800)
                            animate()
                        }
                    }
                ) { change, dragAmount ->
                    change.consumeAllChanges()
                    offsetX += dragAmount.x
                    offsetY += dragAmount.y
                    onMoved(offsetX.roundToInt(), offsetY.roundToInt())
                }
            }
            .click {
                if (canClick) {
                    onExpended(!expand)
                } else {
                    if (offsetX >= context.screenWidth() / 2 - 80) {
                        scope.launch {
                            onMoved(context.screenWidth() / 2 - 80, offsetY.roundToInt())
                            delay(200)
                            onExpended(!expand)
                        }
                    } else if (offsetX <= -context.screenWidth() / 2 + 80) {
                        scope.launch {
                            onMoved(-context.screenWidth() / 2 + 80, offsetY.roundToInt())
                            delay(200)
                            onExpended(!expand)
                        }
                    }
                }
            }
    )
}

@Composable
fun AddIcon(
    open: () -> Unit
){
    Image(
        painter = painterResource(id = R.drawable.add),
        contentDescription = "添加",
        modifier = Modifier
            .size(40.dp)
            .clip(CircleShape)
            .background(color = Color.White)
            .click {
                open()
            }
    )
}