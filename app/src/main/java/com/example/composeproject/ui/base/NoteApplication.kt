package com.example.composeproject.ui.base

import android.app.Activity
import android.app.Application
import android.content.*
import android.os.Bundle
import android.os.IBinder
import com.example.composeproject.database.NoteDataBase
import com.example.composeproject.service.OverlayService
import com.example.composeproject.ui.overlay.OverlayLisenter
import com.example.composeproject.util.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class NoteApplication: Application() {
    val dataBase: NoteDataBase by lazy{ NoteDataBase.getInstance(this) }
    var overlayLisenter: OverlayLisenter? = null
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate() {
        super.onCreate()
        bindService()
        initUser()
        registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks{

            override fun onActivityCreated(p0: Activity, p1: Bundle?) {

            }

            override fun onActivityStarted(p0: Activity) {

            }

            override fun onActivityResumed(p0: Activity) {

            }

            override fun onActivityPaused(p0: Activity) {

            }

            override fun onActivityStopped(p0: Activity) {

            }

            override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

            }

            override fun onActivityDestroyed(p0: Activity) {

            }
        })
    }

    fun getSharedPrefEditInstance(): SharedPreferences{
        val shared: SharedPreferences
        return sharedPreferences ?: synchronized(this){
            shared = this.getSharedPreferences(SETTING_SHARED_PREF, Context.MODE_PRIVATE).apply {
                if(!getBoolean(HAS_INIT,false)){
                    with(this.edit()) {
                        putBoolean(HAS_INIT,true)
                        putBoolean(OVERLAY_VALUE, false)
                        putBoolean(DARK_MODE,false)
                        putInt(NOTE_BACKGROUND,0)
                        apply()
                    }
                }
            }
            sharedPreferences = shared
            shared
        }

    }

    private fun bindService(){
        val intent = Intent(this,OverlayService::class.java)
        bindService(intent,object :ServiceConnection{
            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                overlayLisenter = (p1 as OverlayService.MyServiceBinder).getInstance()
                if(getSharedPrefEditInstance().getBoolean(OVERLAY_VALUE,false)){
                    overlayLisenter?.open()
                }
            }

            override fun onServiceDisconnected(p0: ComponentName?) {

            }

        },BIND_AUTO_CREATE)
    }

    private fun initUser(){
        GlobalScope.launch {
            UserUtil.dao = dataBase.userDao()
            UserUtil.loadUser()
        }
    }
}