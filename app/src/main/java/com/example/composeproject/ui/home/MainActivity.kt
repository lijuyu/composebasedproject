package com.example.composeproject.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle

import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.view.WindowCompat
import com.example.composeproject.NoteApp
import com.example.composeproject.ui.base.BaseActivity
import com.example.composeproject.ui.base.NoteApplication
import com.example.composeproject.ui.home.component.HomeScreen
import com.example.composeproject.ui.theme.ComposeProjectTheme
import com.example.composeproject.util.DARK_MODE
import com.example.composeproject.util.isDark
import com.example.composeproject.util.launch
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.pager.ExperimentalPagerApi

class MainActivity : BaseActivity() {

    companion object {
        fun launch(context: Context,startNavigation: String,isNewTask: Boolean = false){
            context.launch<MainActivity>{
                if(isNewTask){
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                putExtra(START_NAVIGATE,startNavigation)
            }
        }
        const val START_NAVIGATE = "navigate"
    }

    @ExperimentalPagerApi
    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val startNavigation = intent.getStringExtra(START_NAVIGATE)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            var isDark by remember{ mutableStateOf(this.isDark())}
            ComposeProjectTheme(darkTheme = isDark) {
                ProvideWindowInsets(windowInsetsAnimationsEnabled = true) {
                    NoteApp(
                        navigate = startNavigation,
                        themeModeChange = {
                            isDark = it
                            (this.application as NoteApplication).getSharedPrefEditInstance().edit().apply{
                                putBoolean(DARK_MODE,it)
                                apply()
                            }
                        },
                        isDark = isDark
                    )
                }
            }
        }
    }
}

@ExperimentalPagerApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeProjectTheme {
//        NoteApp()
    }
}

