package com.example.composeproject.ui.overlay

interface OverlayLisenter {

    fun open()

    fun close()
}