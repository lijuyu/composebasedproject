package com.example.composeproject.ui.base

import android.os.Bundle
import androidx.activity.ComponentActivity
import com.gyf.immersionbar.ktx.immersionBar

open class BaseActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        immersionBar()
    }
}