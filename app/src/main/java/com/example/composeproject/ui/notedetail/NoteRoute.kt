package com.example.composeproject.ui.notedetail

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.example.composeproject.util.getNoteBg
import com.example.composeproject.util.log
import com.example.composeproject.util.setNoteBg

@ExperimentalMaterialApi
@Composable
fun NoteRoute(
    viewModel: NoteDetailViewModel,
    navigateHome: () -> Unit
) {
    val uiState by viewModel.uiState.collectAsState()
    var noteState by remember{ viewModel.noteState }
    val context = LocalContext.current.applicationContext
    noteState = noteState.copy(bgRes = context.getNoteBg())
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == Activity.RESULT_OK){
            val uri = it.data!!.data
            uri?.let {
                val contentResolver = context.contentResolver
                val int = contentResolver.openInputStream(it)
                val bitmap = BitmapFactory.decodeStream(int)
                viewModel.addImage(bitmap)
            }
        }
    }
    NoteDetailScreen(
        uiState = uiState,
        context = LocalContext.current,
        noteState = noteState,
        changeColorVisible = { noteState = noteState.copy(colorSelectorVisible = it) },
        changeFontVisible = { noteState = noteState.copy(fontSelectorVisible = it) },
        updateFontSize = { noteState = noteState.copy(fontSize = it) },
        updateColor = { noteState = noteState.copy(color = it) },
        fontWeightChange = {
            noteState = if(noteState.isBold){
                noteState.copy(isBold = false)
            }else{
                noteState.copy(isBold = true)
            }
        },
        fontStyleChange = {
            noteState = if(noteState.isItalic){
                noteState.copy(isItalic = false)
            }else{
                noteState.copy(isItalic = true)
            }
        },
        selectImg = {
            val intent = Intent(Intent.ACTION_PICK).apply {
                type = "image/*"
            }
            launcher.launch(intent)
        },
        navigateHome = navigateHome,
        updateTitle = viewModel::updateTile,
        save = viewModel::save,
        notAddImage = viewModel::notAddImage,
        updateSaveState = viewModel::updateSaveState,
        changeBgSelectorVisible = {
            noteState = noteState.copy(
                bgSelectorVisible = it
            )
        },
        changeBackGround = {
            context.setNoteBg(it)
            noteState = noteState.copy(
                bgRes = it
            )
        },
        changeAddTagDialogVisible = {
            noteState = noteState.copy(
                isShowAddTagDialog = it
            )
        },
        addTag = viewModel::addTag,
        deleteTag = viewModel::deleteTag
    )
}