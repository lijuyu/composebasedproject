package com.example.composeproject.ui.timeselect

import androidx.compose.animation.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composeproject.entity.DayEntity
import com.example.composeproject.entity.MonthEntity
import com.example.composeproject.ui.component.AnimatedWithSizeChange
import com.example.composeproject.ui.component.BaseAppBar
import com.example.composeproject.ui.theme.*
import com.example.composeproject.util.*
import java.util.*

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun TimeSelectScreen(
    uiState: TimeUiState,
    selectedMonth: (MonthEntity) -> Unit,
    selectedDay: (DayEntity) -> Unit,
    navigateHome: (Long?) -> Unit
){
    Column(modifier = Modifier.background(color = ModeBgColor())) {
        TimeSelectAppBar(back = {
            navigateHome(null)
        },save = {
            val c = Calendar.getInstance().apply {
                set(Calendar.MONTH,uiState.selectDay.month)
                set(Calendar.DAY_OF_MONTH,uiState.selectDay.day)
            }
            navigateHome(c.time.time)
        })
        Column(modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(start = 30.dp, end = 30.dp)
        ) {
            uiState.list.forEach {
                MonthItem(uiState,it,selectedDay)
            }
        }
    }

}

@Composable
fun TimeSelectAppBar(
    back: () -> Unit,
    save: () -> Unit,
){
    BaseAppBar(back = back, title = "选择时间"){
        Button(
            onClick = {
                save()
            },
        ) {
            Text(text = "保存")
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun MonthItem(
    uiState: TimeUiState,
    currentMonth: MonthEntity,
    selectedDay: (DayEntity) -> Unit,
){
    var expended by remember{ mutableStateOf(currentMonth.isSelect)}
    Surface(
        shape = RoundedCornerShape(10.dp),
        color = ModeColor(),
        onClick = {
            expended = !expended
        },modifier = Modifier.padding(5.dp)
    ) {
        AnimatedWithSizeChange(targetState = expended) {target ->
            Surface(
                modifier = Modifier
                    .border(1.dp, color = PrimaryColor, shape = RoundedCornerShape(10.dp))
                    .fillMaxSize()
                    .background(color = ModeBgColor())
                    .padding(20.dp)
            ) {
                if(!target){
                    UnSelectMonthItem(currentMonth)
                }else{
                    SelectMonthItem(currentMonth,selectedDay,uiState)
                }
            }
        }
    }
}

@Composable
fun UnSelectMonthItem(
    currentMonth: MonthEntity,
){
    Row(modifier = Modifier
        .background(ModeBgColor())
        .fillMaxWidth()
        .height(50.dp)
        .padding(top = 20.dp)
    ) {
        Text(
            text = "${monthInEnglish(currentMonth.month)}",
            fontWeight = FontWeight.Bold,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
        )
    }
}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun SelectMonthItem(currentMonth: MonthEntity, selectedDay: (DayEntity) -> Unit,uiState: TimeUiState){
    val column = currentMonth.days.size  / 7
    var day = 0
    Column(modifier = Modifier
        .background(color = ModeBgColor())
        .fillMaxWidth()
    ) {
        Text(
            text = "${currentMonth.monthInEnglish}",
            fontStyle = FontStyle.Italic,
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp,
            textAlign = TextAlign.End,
            modifier = Modifier
                .fillMaxWidth()
                .padding(end = 10.dp, bottom = 10.dp)
        )
        //星期
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(top = 5.dp)) {
            Row(modifier = Modifier
                .wrapContentWidth()
                .padding(bottom = 10.dp),horizontalArrangement = Arrangement.End,){
                Text(
                    weekInterToString(7),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    color = PrimaryColor,
                    modifier = Modifier.weight(1f)
                )
                for(i in 1..6){
                    Text(
                        weekInterToString(i),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        color = PrimaryColor,
                        modifier = Modifier.weight(1f)
                    )
                }
            }
        }
        //日历
        for(i in 0 until column){
            Row(horizontalArrangement = Arrangement.Start,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Row(modifier = Modifier
                    .wrapContentWidth()
                    .height(30.dp)) {
                    for(i in 1..7){
                        if(currentMonth.days[day].day == 0){
                            Text(
                                "",
                                modifier = Modifier
                                    .weight(1f),
                                textAlign = TextAlign.Center,
                                fontSize = 14.sp
                            )
                        }else{
                            DayItem(uiState = uiState,
                                day = currentMonth.days[day],
                                rowScope = this@Row,
                                selectedDay = selectedDay
                            )
                        }
                        day++
                    }
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun DayItem(
    uiState: TimeUiState,
    day: DayEntity,
    rowScope: RowScope,
    selectedDay: (DayEntity) -> Unit,
){
    val isDark = LocalContext.current.isDark()
    val textColor by animateColorAsState(
        if(uiState.isSelect(day)){
            if(isDark){
                Color.Black
            }else{
                Color.White
            }
        }
        else {
            if(isDark){
                Color.White
            }else{
                Color.Black
            }
        }
    )
    val bgColor by animateColorAsState(
        if(uiState.isSelect(day)) {
            if(isDark){
                Color.White
            }else{
                PrimaryColor
            }
        }
        else{
            Color.Transparent
        }
    )
    with(rowScope){
        Surface(
            shape = CircleShape,color = bgColor,
            modifier = Modifier.weight(1f),
            onClick = {
                if(!(uiState.selectDay.month == day.month && uiState.selectDay.day == day.day)){
                    selectedDay(day)
                }
            }
        ) {
            Text(
                "${day.day}",
                color = textColor,
                textAlign = TextAlign.Center,
                fontSize = 14.sp,
            )
        }
    }
}

