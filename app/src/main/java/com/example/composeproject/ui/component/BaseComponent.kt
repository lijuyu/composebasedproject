package com.example.composeproject.ui.component

import android.app.ActionBar
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.EditText
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.appendInlineContent
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.buildSpannedString
import androidx.core.text.getSpans
import androidx.core.text.inSpans
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.example.composeproject.R
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.ui.notedetail.*
import com.example.composeproject.ui.theme.GainColor
import com.example.composeproject.ui.theme.Grey
import com.example.composeproject.ui.theme.Grey_60
import com.example.composeproject.ui.theme.PrimaryColor
import com.example.composeproject.util.*
import com.google.accompanist.insets.navigationBarsWithImePadding
import kotlinx.coroutines.flow.MutableStateFlow
import java.io.File


@Composable
fun BaseAppBar(
    back: () -> Unit,
    title: String,
    content: @Composable RowScope.() -> Unit = {}
) {
    TopAppBar(
        elevation = 0.dp,
        backgroundColor = Color.Transparent,
        modifier = Modifier.padding(start = 15.dp, end = 15.dp, top = 25.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.back),
            contentDescription = "返回",
            modifier = Modifier
                .size(20.dp)
                .clickable {
                    back()
                }
        )
        Text(
            text = title,
            fontSize = 17.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(start = 15.dp),
            color = ModeTextColor()
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            content(this)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun NoteAppBar(
    back: () -> Unit,
    titleStr: String,
    saveTile: (String) -> Unit,
    content: @Composable RowScope.() -> Unit = {}
) {
    val focus = remember { FocusRequester() }
    var edit by remember { mutableStateOf(false) }
    var title by remember { mutableStateOf("") }
    title = titleStr
    log("笔记:${titleStr}")
    TopAppBar(
        elevation = 0.dp,
        backgroundColor = Color.Transparent,
        modifier = Modifier.padding(start = 15.dp, end = 15.dp, top = 25.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.back),
            contentDescription = "返回",
            modifier = Modifier
                .size(20.dp)
                .clickable {
                    back()
                }
        )
        if (edit) {
            TextField(
                value = title,
                onValueChange = { title = it },
                textStyle = TextStyle(
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold
                ),
                maxLines = 1,
                colors = TextFieldDefaults.textFieldColors(
                    textColor = ModeTextColor(),
                    backgroundColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent
                ),
                placeholder = {Text(text = "输入标题",color = Grey)},
                modifier = Modifier
                    .padding(start = 10.dp)
                    .widthIn(1.dp, Dp.Infinity)
                    .focusRequester(focus)
            )
            LaunchedEffect(key1 = Unit) {
                focus.requestFocus()
            }
        } else {
            if(title.isEmpty()){
                Text(
                    text = "输入标题",
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(start = 15.dp),
                    maxLines = 1,
                    color = Grey
                )
            }else{
                Text(
                    text = title,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(start = 15.dp),
                    maxLines = 1,
                    color = ModeTextColor()
                )
            }
        }
        Surface(color = Color.Transparent,onClick = {
            edit = !edit
            if (!edit) {
                saveTile(title)
            }
        }) {
            if (edit) {
                TiTleEditSure(modifier = Modifier.size(22.dp))
            } else {
                TiTleEdit(modifier = Modifier
                    .size(20.dp))
            }
        }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            content(this)
        }
    }
}

@Composable
fun VerDivider(
    height: Dp,
    width: Dp,
    padding: PaddingValues,
) {
    Box(
        modifier = Modifier
            .height(height)
            .width(width)
            .padding(padding)
            .background(Grey_60)
    )
}

