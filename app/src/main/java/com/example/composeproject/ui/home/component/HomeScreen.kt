package com.example.composeproject.ui.home.component

import android.annotation.SuppressLint
import androidx.activity.compose.BackHandler
import androidx.annotation.LayoutRes
import androidx.compose.animation.*
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.buildSpannedString
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.example.composeproject.R
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.ui.component.*
import com.example.composeproject.ui.home.state.BaseHomeState
import com.example.composeproject.ui.home.viewmodel.HomeUiState
import com.example.composeproject.ui.theme.*
import com.example.composeproject.util.*
import com.google.accompanist.flowlayout.FlowRow
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.roundToInt

enum class SwipeState{
    Open,Close
}

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun HomeScreen(
    uiState: HomeUiState,
    getNote: () -> Unit,
    openNote: (NoteEntity?) -> Unit,
    openTimeSelect: () -> Unit,
    openTags: () -> Unit,
    baseState: BaseHomeState,
    openSetting: () -> Unit,
    getNoteAndTagNum:() ->Unit,
    userInfoShowOnChange: (Boolean) -> Unit,
    changeDeleteState: () -> Unit,
    deleteNote: (NoteEntity?) -> Unit,
    changeNoteToDeleteList: (Boolean,NoteEntity) -> Unit
){
    val listState = rememberLazyListState()
    BackdropScaffold(
        scaffoldState = baseState.backDropScaffoldState,
        modifier = Modifier.padding(top = 20.dp),
        appBar = {
            HomeAppbar(
                uiState = uiState,
                homeState = baseState,
                openTimeSelect = openTimeSelect,
                listState = listState,
                userInfoShowOnChange = userInfoShowOnChange
            )
        },
        backLayerContent = {
            ScaffoldBackLayer(
                openTags = openTags,
                openSetting = openSetting
            )
        },
        frontLayerContent = {
            if(uiState is HomeUiState.HasNotes){
                ScaffoldFrontLayer(
                    uiState = uiState,
                    openNote = openNote,
                    listState = listState,
                    changeDelete = changeDeleteState,
                    changeNoteToDeleteList = changeNoteToDeleteList,
                    homeState = baseState,
                    deleteNote = deleteNote
                )
            }
        },
        gesturesEnabled = true
    )
    Box(contentAlignment = Alignment.BottomEnd,
        modifier = Modifier
            .fillMaxSize()
            .padding(end = 5.dp, bottom = 30.dp)
    ){
        AddNoteFloatingButton{
            openNote(null)
        }
    }
    Box(contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxSize()
    ){
        UserInfoDialog(
            uiState = uiState,
            getNoteAndTagNum = getNoteAndTagNum,
            userInfoShowOnChange = userInfoShowOnChange
        )
    }
    if(uiState is HomeUiState.HasNotes){
        Box(modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 20.dp),
            contentAlignment = Alignment.BottomCenter){
            HomeDeleteButton(
                v = uiState.onDelete,
                changeDeleteState = changeDeleteState){
                deleteNote(null)
            }
        }
    }
}


@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun HomeAppbar(
    uiState: HomeUiState,
    homeState: BaseHomeState,
    openTimeSelect: () -> Unit,
    listState: LazyListState,
    userInfoShowOnChange: (Boolean) -> Unit
){

    TopAppBar(
        elevation = 0.dp,
        modifier = Modifier
            .fillMaxWidth(),
        backgroundColor = Color.Transparent
    ) {
        homeState.imageState.currentValue =
            if(homeState.backDropScaffoldState.isConcealed)
                ImageStateEnum.Init
            else
                ImageStateEnum.Target
        HeadImg(homeState.imageState,modifier = Modifier.padding(start = 10.dp)){
            userInfoShowOnChange(!uiState.showUserInfo)
        }
        Spacer(modifier = Modifier.padding(start = 20.dp))

        if(uiState is HomeUiState.HasNotes){
            HomeAppBarAnimate(targetState = longToDateFormat(uiState.noteList[listState.firstVisibleItemIndex].note.time.time,"yyyy年MM月dd日")) {
                Text(it,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
            }
        }else{
            Text("",
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            )
        }
        Row(horizontalArrangement = Arrangement.End,
            modifier = Modifier
                .background(Color.Transparent)
                .fillMaxWidth()
        ) {
            Surface(onClick = {
                openTimeSelect()
            },shape = CircleShape,color = Color.Transparent) {
                Image(
                    painter = painterResource(id = R.drawable.time),
                    "时间选择",
                    modifier = Modifier
                        .size(22.dp)
                )
            }
        }
    }
}

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Composable
fun ScaffoldFrontLayer(
    uiState: HomeUiState.HasNotes,
    openNote: (NoteEntity?) -> Unit,
    listState: LazyListState,
    changeDelete: () -> Unit,
    changeNoteToDeleteList: (Boolean,NoteEntity) -> Unit,
    deleteNote: (NoteEntity?) -> Unit,
    homeState: BaseHomeState
){
    NoteColumn(
        uiState = uiState,
        openNote = openNote,
        listState = listState,
        changeDelete = changeDelete,
        changeNoteToDeleteList = changeNoteToDeleteList,
        homeState = homeState,
        deleteNote = deleteNote
    )
}

@Composable
fun TimeHeader(time: String){
    Box(modifier = Modifier.background(color = ModeBgColor())){
        Surface(
            color = Color.Transparent,
            shape = RoundedCornerShape(2.dp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 25.dp, top = 10.dp)
                .height(40.dp)
        ) {
            Text(text = time,
                fontSize = 16.sp,
                color = PrimaryColor_60,
                fontWeight = FontWeight.Bold,
            )
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@ExperimentalAnimationApi
@Composable
fun NoteItem(
    note: NoteWithTag,
    openNote: (NoteEntity?) -> Unit,
    changeDelete: () -> Unit,
    deleteNote: (NoteEntity?) -> Unit
){
    var expended by remember{ mutableStateOf(false)}
    val swipeableState = rememberSwipeableState(SwipeState.Close)
    val sizePx = with(LocalDensity.current) {100.dp.toPx() }
    val anchors = mapOf(sizePx to SwipeState.Open,0f to SwipeState.Close)
    Box(modifier = Modifier
        .padding(start = 15.dp,end = 15.dp),
        contentAlignment = Alignment.CenterStart
    ){
        Row(modifier = Modifier
            .height(30.dp)
            .width(100.dp)
            .fillMaxHeight()
        ){
            Box(modifier = Modifier
                .weight(1f),
                contentAlignment = Alignment.Center
            ){
                Image(
                    painter = painterResource(id = R.drawable.upload),
                    contentDescription = "上传",
                    modifier = Modifier.size(30.dp)
                )
            }
            Box(modifier = Modifier
                .weight(1f),
                contentAlignment = Alignment.Center
            ){
                Image(painter = painterResource(id = R.drawable.delete_item),
                    contentDescription = "删除",
                    modifier = Modifier
                        .size(30.dp)
                        .click {
                            deleteNote(note.note)
                        }
                )
            }
        }

        Surface(
            shape = RoundedCornerShape(8.dp),
            elevation = 5.dp,
            color = ModeBgColor(),
            modifier = Modifier
                .offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
                .combinedClickable(
                    onLongClick = {
                        changeDelete()
                    },
                    onClick = {
                        expended = !expended
                    }
                )
                .swipeable(
                    state = swipeableState,
                    anchors = anchors,
                    thresholds = { from, to ->
                        if (to == SwipeState.Open) {
                            FractionalThreshold(0.3f)
                        } else {
                            FractionalThreshold(0.5f)
                        }
                    },
                    orientation = Orientation.Horizontal
                )
        ) {
            GradientBg()
            Column(modifier = Modifier
                .fillMaxWidth()
                .background(color = Color.Transparent)) {
                Text(
                    text = if(note.note.title.isEmpty()) "暂无标题" else note.note.title,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(top = 10.dp,start = 10.dp)
                )
                TagList(tags = note.tagList)
                Spacer(modifier = Modifier.padding(5.dp))
                AnimatedVisibility(visible = expended) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Text(text = note.note.content,
                            maxLines = 6,
                            fontSize = 14.sp,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.padding(10.dp)
                        )
                        Row{
                            Text(text = "打开>>",
                                color = PrimaryColor_60,
                                fontSize = 14.sp,
                                textAlign = TextAlign.End,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .click {
                                        if (expended) {
                                            openNote(note.note)
                                        }
                                    }
                                    .padding(end = 10.dp, bottom = 8.dp)
                            )
                        }
                    }
                }
            }
        }
    }

}

@Composable
fun OnDeleteNoteItem(
    note: NoteWithTag,
    changeNoteToDeleteList: (Boolean,NoteEntity) -> Unit
){
    var value by remember{ mutableStateOf(false)}
    Row(modifier = Modifier.padding(end = 10.dp),verticalAlignment = Alignment.CenterVertically){
        Checkbox(
            checked = value,
            onCheckedChange = {
                value = !value
                changeNoteToDeleteList(value,note.note)
            },colors = CheckboxDefaults.colors(
                checkedColor = PrimaryColor
            )
        )
        Surface(
            shape = RoundedCornerShape(8.dp),
            elevation = 5.dp,
            color = ModeBgColor(),
            modifier = Modifier.click {
                value = !value
                changeNoteToDeleteList(value,note.note)
            }
        ) {
            GradientBg()
            Column(modifier = Modifier
                .fillMaxWidth()
                .background(color = Color.Transparent)) {
                Text(
                    text = if(note.note.title.isEmpty()) "暂无标题" else note.note.title,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(top = 10.dp,start = 10.dp)
                )
                TagList(tags = note.tagList)
                Spacer(modifier = Modifier.padding(5.dp))
            }
        }
    }
}

@ExperimentalMaterialApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Composable
fun NoteColumn(
    listState: LazyListState,
    homeState: BaseHomeState,
    uiState: HomeUiState.HasNotes,
    openNote: (NoteEntity?) -> Unit,
    changeDelete: () -> Unit,
    changeNoteToDeleteList: (Boolean,NoteEntity) -> Unit,
    deleteNote: (NoteEntity?) -> Unit
) {
    val grouped = uiState.noteList.groupBy { longToDateFormat(it.note.time.time, "yyyy年MM月dd日") }
//    val scope = rememberCoroutineScope()
    LazyColumn(
        contentPadding = PaddingValues(vertical = 8.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp),
        state = listState
    ) {
        grouped.forEach {
            item{
                TimeHeader(time = it.key)
            }
            items(
                items = it.value,
                key = { note ->
                    note.note.noteId
                }
            ) { note ->
                if (uiState.onDelete) {
                    OnDeleteNoteItem(note = note, changeNoteToDeleteList = changeNoteToDeleteList)
                } else {
                    NoteItem(
                        note = note,
                        openNote = openNote,
                        changeDelete = changeDelete,
                        deleteNote = deleteNote
                    )
                }
            }
        }
    }
    if (homeState.startTime != 0L) {
        val time = longToDateFormat(homeState.startTime,"yyyy年MM月dd日")
        var index = 0
        grouped.forEach{
            if(time == it.key){
                return@forEach
            }
            index++
        }
        LaunchedEffect(key1 =Unit){
            listState.scrollToItem(index = index)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ScaffoldBackLayer(
    openTags: () -> Unit,
    openSetting: () -> Unit
){
    Box(modifier = Modifier
        .fillMaxWidth()
        .height(100.dp)
    ){
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxHeight()
                .padding(start = 30.dp)
        ){
            homeToolArray.forEach {
                BackToolItem(
                    title = it.key,
                    icon = it.value,
                    action = when(it.key){
                        "标签组" -> openTags
                        "设置" -> openSetting
                        else -> { {} }
                    }
                )
                Spacer(modifier = Modifier.width(50.dp))
            }
        }
    }
}


@SuppressLint("ResourceType")
@Composable
fun BackToolItem(
    title: String,
    @LayoutRes icon:  Int,
    action: () -> Unit
){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.click{
            action()
        }
    ) {
        Image(
            painter = painterResource(id = icon),
            contentDescription = title,
            modifier = Modifier.size(35.dp)
        )
        Spacer(modifier = Modifier.padding(4.dp))
        Text(
            title,
            fontSize = 13.sp,
            color = Color.White
        )
    }
}

@Composable
fun UserInfoDialog(
    uiState: HomeUiState,
    getNoteAndTagNum:() ->Unit,
    userInfoShowOnChange: (Boolean) -> Unit
){
    if(uiState.showUserInfo){
        LaunchedEffect(uiState){
            getNoteAndTagNum()
        }
    }
    AnimatedVisibility(
        visible = uiState.showUserInfo,
        enter = fadeIn() + slideInVertically(),
        exit = fadeOut() + slideOutVertically(),
    ) {
        Box(modifier = Modifier.padding(20.dp)){
            Surface(
                shape = RoundedCornerShape(25.dp),
                elevation = 10.dp,
                modifier = Modifier
                    .height(300.dp)
                    .width(250.dp)
            ) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Spacer(modifier = Modifier.height(50.dp))
                    Image(
                        painter = painterResource(id = R.drawable.male),
                        contentDescription ="头像",
                        modifier = Modifier.size(60.dp)
                    )
                    Spacer(modifier = Modifier.height(50.dp))
                    Row{
                        Box(modifier = Modifier.weight(1f),contentAlignment = Alignment.Center){
                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                Row(verticalAlignment = Alignment.CenterVertically){
                                    Image(
                                        painter = painterResource(id = R.drawable.note),
                                        contentDescription = "笔记数量",
                                        modifier = Modifier.size(40.dp)
                                    )
                                    Text(text = "笔记",fontSize = 16.sp)
                                }
                                Text(text = "${uiState.allNoteNum}",
                                    fontSize = 20.sp,
                                    color = PrimaryColor,
                                    modifier = Modifier.fillMaxWidth(),
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                        Box(modifier = Modifier.weight(1f),contentAlignment = Alignment.Center){
                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                Row(verticalAlignment = Alignment.CenterVertically){
                                    Image(
                                        painter = painterResource(id = R.drawable.tag_num),
                                        contentDescription = "标签数量",
                                        modifier = Modifier.size(40.dp)
                                    )
                                    Text(text = "标签",fontSize = 16.sp)
                                }
                                Text(text = "${uiState.allTagsNum}",
                                    fontSize = 20.sp,
                                    color = PrimaryColor,
                                    modifier = Modifier.fillMaxWidth(),
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                    }
                }
            }
        }
        BackHandler() {
            userInfoShowOnChange(false)
        }
    }
}

@Composable
fun TagList(tags: List<TagEntity>){
    FlowRow(mainAxisSpacing = 5.dp,
        crossAxisSpacing = 8.dp,
        modifier = Modifier
            .padding(top = 10.dp,start = 20.dp)
    ) {
        tags.forEach {
            TagItem(tag = it)
        }
    }
}

@Composable
fun TagItem(tag: TagEntity){
    val color = remember { randomBorder() }
    Surface(
        shape = RoundedCornerShape(10.dp),
        color = ModeBgColor(),
        modifier = Modifier
            .defaultMinSize(minHeight = 20.dp, minWidth = 40.dp)
            .border(width = 1.dp, color = PrimaryColor_35, shape = RoundedCornerShape(10.dp))
    ) {
        Row(verticalAlignment = Alignment.CenterVertically,modifier = Modifier.padding(start = 4.dp)){
            Surface(
                shape = CircleShape,
                color = color.color,
                modifier = Modifier
                    .size(12.dp)
            ) {}
            Text(
                text = tag.name,
                color = Color.Black,
                fontSize = 13.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 8.dp, end = 8.dp)
            )
        }
    }
}

@Composable
fun AddNoteFloatingButton(
    action: () -> Unit
){
    var offsetX by remember { mutableStateOf(0f)}
    var offsetY by remember { mutableStateOf(0f)}
    FloatingActionButton(
        onClick = {
            action()
        },
        shape = CircleShape,
        elevation = FloatingActionButtonDefaults.elevation(defaultElevation = 10.dp),
        backgroundColor = ModeBgColor(),
        modifier = Modifier
            .offset {
                IntOffset(offsetX.roundToInt(), offsetY.roundToInt())
            }
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    change.consumeAllChanges()
                    offsetX += dragAmount.x
                    offsetY += dragAmount.y
                }
            }
            .padding(5.dp),
    ) {
        Image(
            painter = painterResource(id = R.drawable.add_note),
            contentDescription = "记笔记",
            modifier = Modifier.size(26.dp)
        )
    }
}


@Composable
fun RemindDeleteDialog(){

}
