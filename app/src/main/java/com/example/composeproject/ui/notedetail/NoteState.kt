package com.example.composeproject.ui.notedetail

import androidx.annotation.LayoutRes
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.example.composeproject.R
import com.example.composeproject.entity.FontColor

data class NoteState(
    var color: FontColor,
    var fontSize: TextSize,
    var isBold: Boolean,
    var isItalic: Boolean,
    var fontSelectorVisible: Boolean,
    var colorSelectorVisible : Boolean,
    var bgSelectorVisible: Boolean,
    var bgRes: Int,
    var isShowAddTagDialog: Boolean,
){
    fun changeColorVisible(){
//        noteState = this.copy(
//            colorSelectorVisible = !colorSelectorVisible
//        )
        colorSelectorVisible = !colorSelectorVisible
    }
}

fun noteStateAsMutableState(
    color: FontColor = FontColor(value = "#000000",color = Color.Black),
    fontSize: TextSize = TextSize(),
    isBold: Boolean = false,
    isItalic: Boolean = false,
    fontToolVisible: Boolean = false,
    colorSelectorVisible : Boolean = false,
    bgSelectorVisible: Boolean = false,
    bgRes: Int = 0,
    isShowAddTagDialog: Boolean = false
): MutableState<NoteState> {
    return mutableStateOf(
        NoteState(
            color = color,
            fontSize = fontSize,
            isBold = isBold,
            isItalic = isItalic,
            fontSelectorVisible = fontToolVisible,
            colorSelectorVisible = colorSelectorVisible,
            bgSelectorVisible = bgSelectorVisible,
            bgRes = bgRes,
            isShowAddTagDialog = isShowAddTagDialog
        )
    )
}

data class TextSize(
    val value: Int = 25,
    val fontSize: TextUnit = 25.sp
)
enum class PageBg(@LayoutRes val value: Int){
    Red(R.drawable.fuzzy_red),
    Pink(R.drawable.fuzzy_pink),
    Purple(R.drawable.fuzzy_purple),
    Grown(R.drawable.fuzzy_grown),
    Blue(R.drawable.fuzzy_blue),
    Grid(R.drawable.grid_bg),
    StripeHor(R.drawable.stripe_hor),
    StripeVer(R.drawable.stripe_ver),
    Special(R.drawable.special)
}
