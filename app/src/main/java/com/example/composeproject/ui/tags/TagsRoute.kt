package com.example.composeproject.ui.tags

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.example.composeproject.entity.BasicTagEntity
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagerApi
@Composable
fun TagsRoute(
    viewModel: TagsViewModel,
    navigateHome: () -> Unit,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    backParent: (BasicTagEntity) -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
    addTag: (TagEntity) -> Unit,
    changeShowAddTag: (Long?,Boolean) -> Unit
){
    val uiState by viewModel.uiState.collectAsState()
    TagsScreen(
        uiState = uiState,
        navigateHome = navigateHome,
        selectTag = selectTag,
        backParent = backParent,
        addTag = addTag,
        changeShowAddTag = changeShowAddTag,
        navigateNote = navigateNote
    )
}