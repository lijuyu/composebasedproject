package com.example.composeproject.ui.home

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.runtime.*
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.ui.home.component.HomeScreen
import com.example.composeproject.ui.home.state.BaseHomeState
import com.example.composeproject.ui.home.state.rememberHomeState
import com.example.composeproject.ui.home.viewmodel.HomeUiState
import com.example.composeproject.ui.home.viewmodel.HomeViewModel
import com.example.composeproject.util.log

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun HomeRoute(
    viewModel: HomeViewModel,
    navigateTimeSelect: () -> Unit,
    navigateTags: () -> Unit,
    navigateNoteDetail: ( NoteEntity?)-> Unit,
    navigateSetting: () -> Unit,
    time: Long = 0
) {
    val uiState by viewModel.uiState.collectAsState()
    HomeRoute(
        uiState = uiState,
        getNotes = viewModel::getNotes,
        openTimeSelect = navigateTimeSelect,
        openTags = navigateTags,
        baseHomeState = rememberHomeState(startTime = time),
        openNote = navigateNoteDetail,
        openSetting = navigateSetting,
        userInfoShowOnChange = viewModel::userInfoShowOnChange,
        getNoteAndTagNum = viewModel::getTagAndNoteNum,
        changeDeleteState = viewModel::changeDeleteState,
        changeNoteToDeleteList = viewModel::changeNoteToDeleteList,
        deleteNote = viewModel::deleteNote
    )

}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@ExperimentalAnimationApi
@Composable
fun HomeRoute(
    uiState: HomeUiState,
    getNotes: () -> Unit,
    openTimeSelect: () -> Unit,
    openTags: () -> Unit,
    baseHomeState: BaseHomeState,
    openNote: (NoteEntity?) -> Unit,
    openSetting: () -> Unit,
    getNoteAndTagNum:() ->Unit,
    userInfoShowOnChange: (Boolean) -> Unit,
    changeDeleteState: () -> Unit,
    deleteNote: (NoteEntity?) -> Unit,
    changeNoteToDeleteList: (Boolean,NoteEntity) -> Unit,
) {
    HomeScreen(
        uiState = uiState,
        openNote = openNote,
        getNote = getNotes,
        baseState = baseHomeState,
        openTimeSelect = openTimeSelect,
        openTags = openTags,
        openSetting = openSetting,
        getNoteAndTagNum = getNoteAndTagNum,
        userInfoShowOnChange = userInfoShowOnChange,
        changeDeleteState = changeDeleteState,
        deleteNote = deleteNote,
        changeNoteToDeleteList = changeNoteToDeleteList,
    )
}