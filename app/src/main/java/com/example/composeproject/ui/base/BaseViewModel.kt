package com.example.composeproject.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.composeproject.repository.BaseRepository
import com.example.composeproject.repository.HomeRepository
import com.example.composeproject.ui.home.viewmodel.HomeViewModel
import java.lang.reflect.Type


open class BaseViewModel<E>(private val t: E): ViewModel() {



}