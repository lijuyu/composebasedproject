package com.example.composeproject.ui.notedetail

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.Editable
import android.text.Spannable
import android.text.style.ImageSpan
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import com.example.composeproject.R
import com.example.composeproject.entity.ColorEntity
import com.example.composeproject.entity.FontColor
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.service.OverlayService
import com.example.composeproject.ui.component.MyNoteTextField
import com.example.composeproject.ui.component.NoteAppBar
import com.example.composeproject.ui.home.MainActivity
import com.example.composeproject.ui.home.viewmodel.HomeUiState
import com.example.composeproject.ui.theme.*
import com.example.composeproject.util.*
import com.google.accompanist.insets.navigationBarsWithImePadding
import java.io.File

@ExperimentalMaterialApi
@Composable
fun NoteDetailScreen(
    uiState: NoteUiState,
    context: Context,
    noteState: NoteState,
    updateFontSize: (TextSize) -> Unit,
    changeFontVisible: (Boolean) -> Unit,
    changeColorVisible: (Boolean) -> Unit,
    updateColor: (FontColor) -> Unit,
    fontWeightChange: () -> Unit,
    fontStyleChange: () -> Unit,
    selectImg: () -> Unit,
    navigateHome: () -> Unit,
    updateTitle: (String) -> Unit,
    save: (Editable,() -> Unit,() -> Unit) -> Unit,
    notAddImage: () -> Unit,
    updateSaveState: () -> Unit,
    changeBgSelectorVisible: (Boolean) -> Unit,
    changeBackGround: (Int) -> Unit,
    changeAddTagDialogVisible: (Boolean) -> Unit,
    addTag: (TagEntity) -> Unit,
    deleteTag: (TagEntity) -> Unit
) {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(ModeBgColor())
    ){
        if(noteState.bgRes != 0){
            Image(
                painter = painterResource(id = noteState.bgRes),
                contentDescription = "背景",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }
        Column (modifier = Modifier.fillMaxSize()){
            NoteAppBar(
                back = {
                    navigateHome()
                },
                titleStr = uiState.note.title,
                saveTile = {
                    updateTitle(it)
                }
            ){
                Button(onClick = {
                    updateSaveState()
                }) {
                    Text(text = "保存",fontSize = 13.sp)
                }
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 60.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                NoteBody(
                    noteState = noteState,
                    uiState = uiState,
                    context = context,
                    notAddImage = notAddImage,
                    save = save
                )
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.BottomCenter
    ) {
        ToolBottom(
            context = context,
            tags = uiState.tags,
            color = noteState.color.color,
            bgSelectorVisible = noteState.bgSelectorVisible,
            fontVisible = noteState.fontSelectorVisible,
            colorVisible = noteState.colorSelectorVisible,
            selectSize = noteState.fontSize.fontSize,
            updateFontSize = updateFontSize,
            changeFontVisible = changeFontVisible,
            changeColorVisible = changeColorVisible,
            updateColor = updateColor,
            fontStyleChange = fontStyleChange,
            fontWeightChange = fontWeightChange,
            selectFontWeight = noteState.isBold,
            selectFontStyle = noteState.isItalic,
            selectImg = selectImg,
            changeBgSelectorVisible = changeBgSelectorVisible,
            changeBackGround = changeBackGround,
            changeAddTagDialogVisible = changeAddTagDialogVisible,
            deleteTag = deleteTag
        )
    }

    AddTagDialog(
        isShow = noteState.isShowAddTagDialog,
        changeVisible = changeAddTagDialogVisible,
        addTag = addTag
    )
}

@Composable
fun NoteBody(
    context: Context,
    uiState: NoteUiState,
    noteState: NoteState,
    notAddImage: () -> Unit,
    save: (Editable,() -> Unit, () -> Unit) -> Unit,
) {
    Box(
        modifier = Modifier
            .background(color = Color.Transparent)
            .fillMaxSize()
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if(uiState.isInit || uiState.isAddNewNote){
                MyNoteTextField(
                    noteState = noteState,
                    uiState = uiState,
                    changAddImage = notAddImage,
                    save = save,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = Color.Transparent)
                )
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ToolBottom(
    context: Context,
    tags: List<TagEntity>,
    color: Color,
    fontVisible: Boolean,
    bgSelectorVisible: Boolean,
    colorVisible: Boolean,
    selectSize: TextUnit,
    selectFontWeight: Boolean,
    selectFontStyle: Boolean,
    updateFontSize: (TextSize) -> Unit,
    changeFontVisible: (Boolean) -> Unit,
    changeColorVisible: (Boolean) -> Unit,
    updateColor: (FontColor) -> Unit,
    fontWeightChange: () -> Unit,
    fontStyleChange: () -> Unit,
    selectImg: () -> Unit,
    changeBgSelectorVisible: (Boolean) -> Unit,
    changeBackGround: (Int) -> Unit,
    changeAddTagDialogVisible: (Boolean) -> Unit,
    deleteTag: (TagEntity) -> Unit
) {
    Column(modifier = Modifier
        .background(color = Color.Transparent)
        .navigationBarsWithImePadding()
    ) {
        Box() {
            ColorSelector(
                visible = colorVisible,
                updateColor = updateColor,
                changeColorVisible = changeColorVisible
            )
            FontSelector(
                visible = fontVisible,
                updateFontSize = updateFontSize,
                selectSize = selectSize,
                selectFontStyle = selectFontStyle,
                selectFontWeight = selectFontWeight,
                fontStyleChange = fontStyleChange,
                fontWeightChange = fontWeightChange
            )
        }
        Column(modifier = Modifier.background(color = ModeBgColor())) {
            TagTool(
                tags = tags,
                changeAddTagDialogVisible = changeAddTagDialogVisible,
                deleteTag = deleteTag
            )
            Row(
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp, top = 5.dp, bottom = 5.dp)
            ) {
                Box(
                    modifier = Modifier
                        .weight(1f),
                    contentAlignment = Alignment.Center
                ) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        BorderColorCirCle(
                            colorVisible = colorVisible,
                            color = color,
                            changeColorVisible = {
                                if (fontVisible) {
                                    changeFontVisible(false)
                                }
                                changeColorVisible(!colorVisible)
                            }
                        )
                        Text(text = "颜色", fontSize = 13.sp)
                    }
                }
                toolMap().forEach {
                    ToolItem(
                        tip = it.key.value,
                        painter = it.value,
                        modifier = Modifier
                            .weight(1f)
                            .click {
                                when (it.key) {
                                    ClickEnum.Image -> {
                                        selectImg()
                                    }
                                    ClickEnum.Font -> {
                                        if (colorVisible) {
                                            changeColorVisible(false)
                                        }
                                        changeFontVisible(!fontVisible)
                                    }
                                    ClickEnum.Background -> {
                                        //todo 显示背景选择
                                        if (colorVisible) {
                                            changeColorVisible(false)
                                        }
                                        if (fontVisible) {
                                            changeFontVisible(false)
                                        }
                                        changeBgSelectorVisible(!bgSelectorVisible)
                                    }
                                }
                            }
                    )
                }
            }
        }
        Box(modifier = Modifier.background(color = ModeBgColor())){
            PageBgTool(
                isShow = bgSelectorVisible,
                changeBgSelectorVisible = changeBgSelectorVisible,
                changeBackGround = changeBackGround
            )
        }
    }
}

@Composable
fun FontSelector(
    selectSize: TextUnit,
    visible: Boolean,
    selectFontWeight: Boolean,
    selectFontStyle: Boolean,
    updateFontSize: (TextSize) -> Unit,
    fontWeightChange: () -> Unit,
    fontStyleChange: () -> Unit
) {
    with(LocalDensity.current){
        AnimatedVisibility(visible = visible, modifier = Modifier.fillMaxWidth()) {
            Surface(
                shape = RoundedCornerShape(topStart = 10.dp,topEnd = 10.dp),
                color = ModeBgColor()
            ) {
                Surface(
                    shape = RoundedCornerShape(topStart = 10.dp,topEnd = 10.dp),
                    color = Grey_60,
//                    modifier = Modifier.padding(start = 10.dp, end = 10.dp)
                ) {
                    Column {
                        Row(modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp)) {
                            FontWeightItem(
                                isSelect = selectFontWeight,
                                fontWeightChange = fontWeightChange
                            )
                            Spacer(modifier = Modifier.padding(start = 8.dp))
                            FontStyleItem(
                                isSelect = selectFontStyle,
                                fontStyleChange = fontStyleChange
                            )
                        }
                        Row(
                            verticalAlignment = Alignment.Bottom,
                            modifier = Modifier
                                .padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 10.dp)
                        ) {
                            Row(
                                modifier = Modifier
                                    .clip(RoundedCornerShape(8.dp))
                                    .background(color = DimGrey_30)
                                    .padding(top = 10.dp, bottom = 10.dp),
                                verticalAlignment = Alignment.Bottom,
                            ) {
                                fontSizes.forEach {
                                    FontItem(
                                        rowScope = this,
                                        isSelect = it.value == selectSize,
                                        size = it.key
                                    ) {
                                        updateFontSize(TextSize(fontSize = it.value,value = it.value.roundToPx()))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

@Composable
fun ColorSelector(
    visible: Boolean,
    changeColorVisible: (Boolean) -> Unit,
    updateColor: (FontColor) -> Unit
) {
    AnimatedVisibility(visible = visible, modifier = Modifier.fillMaxWidth()) {
        Surface(
            shape = RoundedCornerShape(topStart = 10.dp,topEnd = 10.dp),
            color = ModeBgColor()
        ) {
            Surface(
                shape = RoundedCornerShape(topStart = 10.dp,topEnd = 10.dp),
                color = Grey_60,
            ) {
                Row(
                    modifier = Modifier
                        .padding(top = 10.dp, bottom = 10.dp)
                        .fillMaxWidth()
                ) {
                    textColor.forEach {
                        Box(
                            modifier = Modifier
                                .weight(1f)
                                .padding(top = 8.dp, bottom = 8.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            ColorCirCle(
                                color = it,
                                updateColor = {
                                    updateColor(it)
                                }, changeColorVisible = changeColorVisible
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun FontWeightItem(
    isSelect: Boolean,
    fontWeightChange: () -> Unit,
) {
    val transition = updateTransition(targetState = isSelect, label = "")
    val color by transition.animateColor(
        transitionSpec = {
            if (isSelect) spring(5f)
            else spring(5f)
        }
    ) { color ->
        if (color) {
            DimGrey_30
        } else {
            Color.Transparent
        }
    }
    Box(modifier = Modifier
        .clip(RoundedCornerShape(4.dp))
        .background(color = color)
        .padding(4.dp)
        .click {
            fontWeightChange()
        }) {
        Image(
            painter = painterResource(id = R.drawable.font_weight),
            contentDescription = "字体加粗",
            modifier = Modifier
                .size(20.dp)
        )
    }
}

@Composable
fun FontStyleItem(
    isSelect: Boolean,
    fontStyleChange: () -> Unit
) {
    val transition = updateTransition(targetState = isSelect, label = "")
    val color by transition.animateColor(
        transitionSpec = {
            if (isSelect) spring(5f)
            else spring(5f)
        }
    ) { color ->
        if (color) {
            DimGrey_30
        } else {
            Color.Transparent
        }
    }
    Box(modifier = Modifier
        .clip(RoundedCornerShape(4.dp))
        .background(color = color)
        .padding(4.dp)
        .click {
            fontStyleChange()
        }) {
        Image(
            painter = painterResource(id = R.drawable.font_itic),
            contentDescription = "字体倾斜",
            modifier = Modifier
                .size(20.dp)
        )
    }
}


@Composable
fun FontItem(
    rowScope: RowScope,
    isSelect: Boolean,
    size: Dp,
    onClick: () -> Unit
) {
    val transition = updateTransition(targetState = isSelect, label = "")
    val width by transition.animateDp() { width ->
        if (width) {
            size
        } else {
            0.dp
        }
    }
    with(rowScope) {
        if (isSelect) {
            Box(
                modifier = Modifier
                    .weight(1f)
                    .click {
                        onClick()
                    }, contentAlignment = Alignment.Center
            ) {
                Column {
                    Image(
                        painter = painterResource(id = R.drawable.font),
                        contentDescription = "字体",
                        modifier = Modifier.size(size)
                    )
                    Divider(color = Color.Black, modifier = Modifier.width(width = width))
                }
            }
        } else {
            Image(
                painter = painterResource(id = R.drawable.font),
                contentDescription = "字体",
                modifier = Modifier
                    .size(size)
                    .weight(1f)
                    .click {
                        onClick()
                    }
            )
        }
    }
}

@Composable
fun BorderColorCirCle(
    colorVisible: Boolean,
    color: Color,
    changeColorVisible: (Boolean) -> Unit
) {
    Surface(
        shape = CircleShape,
        color = ModeColor(),
        modifier = Modifier
            .size(25.dp)
            .click {
                changeColorVisible(!colorVisible)
            }
            .border(width = 2.dp, color = color, shape = CircleShape)
            .padding(5.dp)
    ) {
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .background(color = color)
        )
    }
}

@Composable
fun ColorCirCle(
    color: FontColor,
    updateColor: (FontColor) -> Unit = {},
    changeColorVisible: (Boolean) -> Unit
) {
    Surface(
        shape = CircleShape,
        color = color.color,
        modifier = Modifier
            .size(25.dp)
            .click {
                changeColorVisible(false)
                updateColor(color)
            }
    ) {

    }
}

@Composable
fun ToolItem(
    tip: String,
    painter: Painter,
    modifier: Modifier,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Image(
            painter = painter,
            contentDescription = tip,
            modifier = Modifier.size(25.dp)
        )
        Text(text = tip, fontSize = 13.sp)
    }
}

@Composable
fun PageBgTool(
    isShow: Boolean,
    changeBgSelectorVisible: (Boolean) -> Unit,
    changeBackGround: (Int) -> Unit
){
    val heightAsState by animateDpAsState(targetValue = if(isShow) 250.dp else 0.dp)
    Box(modifier = Modifier
        .fillMaxWidth()
        .height(heightAsState)
        .background(color = Grey_60)
        .padding(20.dp)
    ){
        Column() {
            Text(text = "选择背景:",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Start,
                fontSize = 15.sp,
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text(text = "纯色背景:",
                textAlign = TextAlign.Start,
                fontSize = 15.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 30.dp)
            )
            Spacer(modifier = Modifier.height(10.dp))
            Row{
                Column(modifier = Modifier.weight(1f),horizontalAlignment = Alignment.CenterHorizontally) {
                    Surface(
                        color = ModeBgColor(),
                        shape = CircleShape,
                        modifier = Modifier
                            .size(20.dp)
                            .click {
                                if (isShow) {
                                    changeBackGround(0)
                                    changeBgSelectorVisible(false)
                                }
                            }
                    ) {}
                    Text(text = "(恢复)",
                        fontSize = 13.sp,
                        color = TextColor_Black_60,
                    )
                }
                colorBg.forEach {
                    Column(modifier = Modifier.weight(1f),horizontalAlignment = Alignment.CenterHorizontally) {
                        Surface(
                            color = it.key,
                            shape = CircleShape,
                            modifier = Modifier
                                .size(20.dp)
                                .click {
                                    if (isShow) {
                                        changeBackGround(it.value)
                                        changeBgSelectorVisible(false)
                                    }
                                }
                        ) {}
                        Text(text = "(磨砂)",
                            fontSize = 13.sp,
                            color = TextColor_Black_60,
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(20.dp))
            Text(text = "图案背景:",
                textAlign = TextAlign.Start,
                fontSize = 15.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 30.dp)
            )
            Spacer(modifier = Modifier.height(10.dp))
            Row{
                specialBg().forEach{
                    Box(modifier = Modifier.weight(1f),contentAlignment = Alignment.Center){
                        Image(
                            painter = painterResource(id = it.value),
                            contentDescription = null,
                            contentScale = ContentScale.Crop,
                            modifier = Modifier
                                .size(60.dp)
                                .clip(RoundedCornerShape(5.dp))
                                .click {
                                    if (isShow) {
                                        changeBackGround(it.value)
                                        changeBgSelectorVisible(false)
                                    }
                                }
                        )
                    }
                }
            }
        }
    }
    if(isShow){
        BackHandler() {
            changeBgSelectorVisible(false)
        }
    }
}

@Composable
fun TagTool(
    tags: List<TagEntity>,
    changeAddTagDialogVisible: (Boolean) -> Unit,
    deleteTag: (TagEntity) -> Unit
){
    Box{
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .padding(start = 10.dp),
            verticalAlignment = Alignment.CenterVertically
        ){
            Image(
                painter = painterResource(id = R.drawable.tag_num),
                contentDescription = "标签列表",
                modifier = Modifier.size(25.dp)
            )
            Box(modifier = Modifier.padding(end = 50.dp)){
                Row(modifier = Modifier
                    .horizontalScroll(rememberScrollState())
                    .padding(start = 20.dp, end = 20.dp)
                ){
                    tags.forEach {
                        NoteTagItem(
                            tag = it,
                            deleteTag = deleteTag
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                    }
                }
            }
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .padding(start = 25.dp)){
            GradientHorToRight(modifier = Modifier
                .height(40.dp)
                .width(30.dp)
            )
        }
        Box(modifier = Modifier
            .background(Color.Transparent)
            .fillMaxWidth(),
            contentAlignment = Alignment.CenterEnd
        ){
            Box(modifier = Modifier
                .width(80.dp)
                .height(40.dp),contentAlignment = Alignment.CenterStart){
                Row(verticalAlignment = Alignment.CenterVertically){
                    GradientHorToLeft(modifier = Modifier
                        .height(40.dp)
                        .width(30.dp))
                    Box(modifier = Modifier.padding(start = 5.dp)){
                        AddTagImg(modifier = Modifier
                            .size(20.dp)
                            .click {
                                changeAddTagDialogVisible(true)
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun NoteTagItem(
    tag:TagEntity,
    deleteTag: (TagEntity) -> Unit
){
    val color = remember { randomColor() }
    val textColor = remember {
        if (color.isLight) TextColor_Black_60 else TextWhiteColor
    }
    Surface(
        color = color.color,
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier
            .defaultMinSize(minHeight = 20.dp, minWidth = 40.dp),
    ) {
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(end = 5.dp)
        ){
            Text(
                text = tag.name,
                color = textColor,
                fontSize = 12.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 8.dp,end = 2.dp)
            )
            Image(
                painter = painterResource(id = R.drawable.dismiss),
                contentDescription = null,
                modifier = Modifier
                    .size(12.dp)
                    .click {
                        deleteTag(tag)
                    }
            )
        }
    }
}

@Composable
fun AddTagDialog(
    isShow: Boolean,
    changeVisible: (Boolean) -> Unit,
    addTag:(TagEntity) -> Unit
){
    var text by remember{ mutableStateOf("")}
    AnimatedVisibility(
        visible = isShow,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Box(modifier = Modifier
            .background(color = Black_60)
            .fillMaxSize(),
            contentAlignment = Alignment.Center
        ){
            Surface(
                shape = RoundedCornerShape(15.dp),
                modifier = Modifier
                    .width(200.dp)
                    .height(130.dp),
            ) {
                Column() {
                    TextField(
                        value = text,
                        onValueChange = {
                            text = it
                        },colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = GainColor,
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent
                        ),
                        shape = RoundedCornerShape(10.dp),
                        label = {Text("标签",color = PrimaryColor,fontSize = 13.sp,modifier = Modifier.alpha(0.6f))},
                        modifier = Modifier.padding(top = 5.dp,start = 10.dp,end = 10.dp,bottom = 5.dp)
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Row{
                        Text(text = "取消",
                            textAlign = TextAlign.Center,
                            color = PrimaryColor,
                            modifier = Modifier
                                .weight(1f)
                                .click { changeVisible(false) }
                        )
                        Text(text = "添加",
                            textAlign = TextAlign.Center,
                            color = PrimaryColor,
                            modifier = Modifier
                                .weight(1f)
                                .click {
                                    if (isShow) {
                                        val tagEntity = TagEntity(name = text)
                                        addTag(tagEntity)
                                        changeVisible(false)
                                    }
                                }
                        )
                    }
                }
            }
        }
        BackHandler() {
            changeVisible(false)
        }
    }
    
}





