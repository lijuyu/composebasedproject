package com.example.composeproject.ui.setting

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

data class SettingState(
    var request: Boolean = false,
)
