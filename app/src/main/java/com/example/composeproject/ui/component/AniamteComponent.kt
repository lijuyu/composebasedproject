package com.example.composeproject.ui.component

import androidx.compose.animation.*
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntSize
import com.example.composeproject.util.ModeBgColor

@ExperimentalAnimationApi
@Composable
fun AnimatedWithSizeChange(
    targetState: Boolean,
    content:@Composable AnimatedVisibilityScope.(Boolean) -> Unit
){
    AnimatedContent(
        targetState = targetState,
        transitionSpec = {
            fadeIn(animationSpec = tween(250, 250)) with
                    fadeOut(animationSpec = tween(250)) using
                    SizeTransform { initialSize, targetSize ->
                        if (targetState) {
                            keyframes {
                                IntSize(targetSize.width, initialSize.height) at 250
                                durationMillis = 500
                            }
                        } else {
                            keyframes {
                                IntSize(initialSize.width, targetSize.height) at 250
                                durationMillis = 500
                            }
                        }
                    }
        },
        modifier = Modifier.background(color = ModeBgColor())
    ) {target ->
        content(target)
    }
}

@ExperimentalAnimationApi
@Composable
fun HomeAppBarAnimate(
    targetState: String,
    content: @Composable (String) -> Unit
){
    AnimatedContent(
        targetState = targetState,
        transitionSpec = {
            if (targetState > initialState) {
                slideInVertically { height -> height } + fadeIn() with
                        slideOutVertically { height -> -height } + fadeOut()
            } else {
                slideInVertically { height -> -height } + fadeIn() with
                        slideOutVertically { height -> height } + fadeOut()
            }.using(
                SizeTransform(clip = false)
            )
        }
    ) { targetCount ->
        content(targetCount)
    }
}