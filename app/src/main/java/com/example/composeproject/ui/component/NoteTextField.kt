package com.example.composeproject.ui.component

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.*
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.text.style.ImageSpan
import android.text.style.StyleSpan
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.core.text.toHtml
import androidx.core.text.toSpannable
import com.example.composeproject.R
import com.example.composeproject.entity.ContentType
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.ui.notedetail.NoteState
import com.example.composeproject.ui.notedetail.NoteUiState
import com.example.composeproject.util.*
import com.google.accompanist.insets.navigationBarsWithImePadding
import java.util.*

@RequiresApi(Build.VERSION_CODES.Q)
@Composable
fun MyNoteTextField(
    uiState: NoteUiState,
    noteState: NoteState,
    modifier: Modifier = Modifier,
    changAddImage: () -> Unit,
    save: (Editable,() -> Unit,() -> Unit) -> Unit,
) {
    val watcher = remember {
        MyTextWatcher(changeAddImage = changAddImage).apply {
            textColor = noteState.color
            fontSize = noteState.fontSize
            isBold = noteState.isBold
            isItalic = noteState.isItalic
        }
    }
    AndroidView(factory = { ctx ->
        EditText(ctx).apply {
            val params = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            background = null
            textCursorDrawable = ctx.getDrawable(R.drawable.cursor)
            if(uiState.noteTextList.isNotEmpty()){
                val span = SpannableStringBuilder()
                uiState.noteTextList.forEach {
                    when(it.type){
                        ContentType.Text ->{
                            val s = SpannableStringBuilder(it.text)
                            s.setSpan(ForegroundColorSpan(Color.parseColor(it.textColor)),0,it.text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            if(it.isBold){
                                s.setSpan(StyleSpan(Typeface.BOLD),0,it.text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            }
                            if(it.isItalic){
                                s.setSpan(StyleSpan(Typeface.ITALIC),0,it.text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            }
                            val size = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,it.fontSize.toFloat(),resources.displayMetrics);
                            s.setSpan(AbsoluteSizeSpan(size.toInt()),0,it.text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            span.append(s)
                        }
                        ContentType.Image ->{
                            val s = SpannableStringBuilder(it.path)
                            val html = Html.fromHtml("<img src=${it.path}></img>",Html.FROM_HTML_MODE_COMPACT,
                                ImageGetter(context,it.bitmap!!),null)
                            val i = html.getSpans(0,html.length,ImageSpan::class.java)
                            s.setSpan(i[0],0,it.path.length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            span.append(s)
                        }
                    }
                }
                text = span
            }
            watcher.editText = this
            addTextChangedListener(watcher)
            layoutParams = params
        }
    },modifier = modifier.navigationBarsWithImePadding()) { edit ->
        edit.apply {
            if(uiState.isAddImage){
                val time = Calendar.getInstance().time.time
                watcher.isAddImage = true
                watcher.bitmap = uiState.bitmap
                edit.text.appendRange("$time",0,"$time".length)
            }
            watcher.apply {
                textColor = noteState.color
                fontSize = noteState.fontSize
                isBold = noteState.isBold
                isItalic = noteState.isItalic
            }
            if(uiState.isSave){
                save(text,{
                    Toast.makeText(context, "保存失败", Toast.LENGTH_SHORT).show()
                },{
                    Toast.makeText(context, "保存成功", Toast.LENGTH_SHORT).show()
                })
            }
        }
    }
}





