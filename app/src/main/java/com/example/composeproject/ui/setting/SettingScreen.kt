package com.example.composeproject.ui.setting

import android.Manifest
import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.compose.BackHandler
import androidx.annotation.RequiresApi
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.composeproject.ui.base.NoteApplication
import com.example.composeproject.ui.component.BaseAppBar
import com.example.composeproject.ui.theme.DarkPrimaryColor
import com.example.composeproject.ui.theme.Grey_60
import com.example.composeproject.ui.theme.PrimaryColor
import com.example.composeproject.ui.theme.SwitchPrimaryColor
import com.example.composeproject.util.*
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState

@Composable
fun SettingScreen(
    state: SettingState,
    isShowOverlay: Boolean,
    backHome: () -> Unit,
    onOverlayChange: (Boolean) -> Unit,
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean,
    requestLaunchPermission: () -> Unit,
    onPermissionChange: (Boolean) -> Unit,
) {
    Box(contentAlignment = Alignment.Center){
        if(isDark){
            DarkScreen(
                isShowOverlay = isShowOverlay,
                backHome = backHome,
                onOverlayChange = onOverlayChange,
                themeModeChange = themeModeChange,
                showPermissionWindow = onPermissionChange
            )
        }else{
            LightScreen(
                isShowOverlay = isShowOverlay,
                backHome = backHome,
                onOverlayChange = onOverlayChange,
                themeModeChange = themeModeChange,
                showPermissionWindow = onPermissionChange
            )
        }
        PermissionRequestWindow(
            vis = state.request,
            onVisChange = onPermissionChange,
            requestLaunchPermission = requestLaunchPermission
        )
    }
}

@Composable
fun SettingColumn(
    isShowOverlay: Boolean,
    onOverlayChange: (Boolean) -> Unit,
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean,
    showPermissionWindow: (Boolean) -> Unit
){
    Column(modifier = Modifier
        .fillMaxSize()
        .background(color = ModeBgColorGrey())
    ) {
        settingArrays.forEach {
            when(it){
                "悬浮窗" -> {
                    OverLay(
                        isShowOverlay = isShowOverlay,
                        onOverlayChange = onOverlayChange,
                        showPermissionWindow = showPermissionWindow
                    )
                }
                "深夜模式" ->{
                    DarkItem(
                        isDark = isDark,
                        themeModeChange = themeModeChange)
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
        }
    }
}


@Composable
fun SettingItem(
    title: String,
    content: @Composable () -> Unit,
    action: () -> Unit
){
    Box{
        Row(
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .background(color = ModeBgColor())
                .fillMaxWidth()
                .height(70.dp)
                .padding(top = 8.dp, bottom = 8.dp, start = 15.dp, end = 10.dp)
        ) {
            content()
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .background(color = Color.Transparent)
                .height(70.dp)
                .fillMaxWidth()
                .padding(top = 8.dp, bottom = 8.dp, start = 15.dp, end = 10.dp),
        ) {
            Text(text = title,color = ModeTextColor())
        }
    }
}

@Composable
fun OverLay(
    isShowOverlay: Boolean,
    onOverlayChange: (Boolean) -> Unit,
    showPermissionWindow: (Boolean) -> Unit
){
    var isShow by remember{ mutableStateOf(isShowOverlay)}
    val context = LocalContext.current
    SettingItem(
        title = "悬浮窗",
        content = {
            Switch(
                checked = isShow,
                onCheckedChange = {
                    if(context.checkOverlayPermission()){
                        isShow = !isShow
                        onOverlayChange(it)
                    }else{
                        showPermissionWindow(true)
                        isShow = false
                    }
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = SwitchCheckedThumbColor(),
                    checkedTrackColor = SwitchCheckedTrackColor(),
                    uncheckedThumbColor = SwitchUnCheckedThumbColor(),
                    uncheckedTrackColor = SwitchUnCheckedTrackColor(),
                )
            )
        },
        action = {}
    )
}

@Composable
fun DarkItem(
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean
){
    SettingItem(
        title = "深夜模式",
        content = {
            Switch(
                checked = isDark,
                onCheckedChange = {
                    themeModeChange(it)
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = SwitchCheckedThumbColor(),
                    checkedTrackColor = SwitchCheckedTrackColor(),
                    uncheckedThumbColor = SwitchUnCheckedThumbColor(),
                    uncheckedTrackColor = SwitchUnCheckedTrackColor(),
                )
            )
        },
        action = {}
    )
}

@Composable
fun DarkScreen(
    isShowOverlay: Boolean,
    backHome: () -> Unit,
    onOverlayChange: (Boolean) -> Unit,
    themeModeChange: (Boolean) -> Unit,
    showPermissionWindow: (Boolean) -> Unit
) {
    Column(Modifier.background(color = ModeBgColor())){
        BaseAppBar(back = {
            backHome()
        }, title = "设置")
        Divider(
            color = ModeBgColorGrey(),
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .padding(start = 10.dp, end = 10.dp)
        )
        SettingColumn(
            isShowOverlay = isShowOverlay,
            onOverlayChange = onOverlayChange,
            themeModeChange = themeModeChange,
            isDark = true,
            showPermissionWindow = showPermissionWindow
        )
    }
}

@Composable
fun LightScreen(
    isShowOverlay: Boolean,
    backHome: () -> Unit,
    onOverlayChange: (Boolean) -> Unit,
    themeModeChange: (Boolean) -> Unit,
    showPermissionWindow: (Boolean) -> Unit
){
    Column(Modifier.background(color = ModeBgColor())){
        BaseAppBar(back = {
            backHome()
        }, title = "设置")
        Divider(
            color = ModeBgColorGrey(),
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
                .padding(start = 10.dp, end = 10.dp)
        )
        SettingColumn(
            isShowOverlay = isShowOverlay,
            onOverlayChange = onOverlayChange,
            themeModeChange = themeModeChange,
            isDark = false,
            showPermissionWindow = showPermissionWindow
        )
    }
}

@Composable
fun PermissionRequestWindow(
    vis: Boolean,
    onVisChange: (Boolean) -> Unit,
    requestLaunchPermission: () -> Unit,
){
    AnimatedVisibility(
        visible = vis,
        enter = fadeIn() + slideInVertically(),
        exit = fadeOut() + slideOutVertically(),
    ) {
        Box(modifier = Modifier.padding(20.dp)){
            Surface(shape = RoundedCornerShape(15.dp),
                elevation = 10.dp,
                modifier = Modifier
                    .height(140.dp)
                    .width(220.dp)
            ){
                Column{
                    Text(
                        text = "打开悬浮窗需要您开启相应权限,请您打开相关权限",
                        modifier = Modifier
                            .padding(top = 20.dp,start = 20.dp,bottom = 10.dp,end = 10.dp)
                    )
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier
                            .padding(top = 10.dp,start = 20.dp,bottom = 10.dp,end = 10.dp)
                    ) {
                        Box(contentAlignment = Alignment.Center,modifier = Modifier.weight(1f)){
                            Text(
                                text = "取消",
                                color = ModeTextColor(),
                                modifier = Modifier
                                    .click {
                                        onVisChange(false)
                                    }
                            )
                        }
                        Box(contentAlignment = Alignment.Center,modifier = Modifier.weight(1f)){
                            Text(
                                text = "开启",
                                color = ModeTextColor(),
                                modifier = Modifier
                                    .click {
                                        onVisChange(false)
                                        requestLaunchPermission()
                                    }
                            )
                        }
                    }
                }
            }
        }
        BackHandler {
            onVisChange(false)
        }
    }
}