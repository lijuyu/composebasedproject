package com.example.composeproject.ui.setting

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import com.google.accompanist.permissions.ExperimentalPermissionsApi


@Composable
fun SettingRoute(
    isShowOverlay: Boolean,
    navigateHome: () -> Unit,
    onOverlayChange: (Boolean) -> Unit,
    themeModeChange: (Boolean) -> Unit,
    isDark: Boolean = false
){
    var state by remember { mutableStateOf(SettingState()) }
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult()
    ){

    }
    val context = LocalContext.current
    SettingScreen(
        state = state,
        isShowOverlay = isShowOverlay,
        backHome = navigateHome,
        onOverlayChange = onOverlayChange,
        themeModeChange = themeModeChange,
        isDark = isDark,
        requestLaunchPermission = {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION).apply {
                data = Uri.parse("package:" + context.packageName)
            }
            launcher.launch(intent)
        },
        onPermissionChange = {
            state = state.copy(request = it)
        }
    )
}