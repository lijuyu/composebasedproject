package com.example.composeproject.ui.component

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.example.composeproject.R
import com.example.composeproject.ui.theme.HeadBorderColor
import com.example.composeproject.util.ModeBgColor
import com.example.composeproject.util.UserUtil
import com.example.composeproject.util.log

@Composable
fun HeadImg(
    state: ImageState = remember { ImageState() },
    modifier: Modifier,
    onClick: () -> Unit
){
    val size by animateDpAsState(
        if(state.currentValue == ImageStateEnum.Target)
            state.targetSize
        else
            state.initSize)
    Image(painter =
    painterResource(
        id = R.drawable.male
    ), contentDescription = "默认头像",
        modifier = modifier
            .border(width = 0.5.dp, color = HeadBorderColor, shape = CircleShape)
            .clip(CircleShape)
            .size(size)
            .clickable { onClick() }
    )
}

@Composable
fun GradientBg(){
    Box(contentAlignment = Alignment.CenterEnd,
        modifier = Modifier
            .fillMaxWidth()
            .padding(end = 15.dp)
    ){
        Image(painter = painterResource(id = R.drawable.note_item),
            contentDescription = null,
            modifier = Modifier
                .size(50.dp),
            contentScale = ContentScale.Crop
        )
        NoteLayer(modifier = Modifier.size(50.dp))
    }
}

@Composable
fun NoteLayer(modifier: Modifier = Modifier){
    val color = ModeBgColor()
    Canvas(modifier = modifier){
        val deHeight = size.height / 100
        val deAlpha = 1.0f / 100
        for(i in 0..100){
            drawRect(
                color = color,
                size = Size(width = size.width,height = deHeight),
                alpha = deAlpha * i,
                topLeft = Offset(0f,deHeight * i)
            )
        }
    }
}

data class ImageState(
    val initSize: Dp = 30.dp,
    val targetSize: Dp = 45.dp,
    var currentValue: ImageStateEnum = ImageStateEnum.Init
)

enum class ImageStateEnum{
    Init,Target
}