package com.example.composeproject.ui.setting

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.composeproject.entity.UploadEntity
import com.example.composeproject.repository.SettingRepository
import kotlinx.coroutines.launch

class SettingViewModel(val repository: SettingRepository): ViewModel() {
    var data: UploadEntity? = null

    fun upload(entity: UploadEntity){
        viewModelScope.launch {
            data = repository.getData()
            repository.upload(data!!)
        }
    }

    private fun getData(){
        viewModelScope.launch {
            val notes = repository.getData()
        }
    }

    companion object{
        fun provideFactory(
            repository: SettingRepository,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return SettingViewModel(repository) as T
            }
        }
    }
}