package com.example.composeproject.ui.tags

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composeproject.R
import com.example.composeproject.entity.BasicTagEntity
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import com.example.composeproject.ui.component.BaseAppBar
import com.example.composeproject.ui.theme.*
import com.example.composeproject.util.*
import com.google.accompanist.flowlayout.FlowRow
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState

@ExperimentalPagerApi
@Composable
fun TagsScreen(
    uiState: TagsUiState,
    navigateHome: () -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    backParent: (BasicTagEntity) -> Unit,
    addTag: (TagEntity) -> Unit,
    changeShowAddTag: (Long?,Boolean) -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            BaseAppBar(back = navigateHome, title = "标签组"){
                Button(onClick = {
                    changeShowAddTag(null,true)
                }) {
                    Text(
                        text = "添加",
                        fontSize = 14.sp
                    )
                }
            }
            TagItemList(
                selectTag = selectTag,
                backParent = backParent,
                tags = uiState.tagList,
                changeShowAddTag = changeShowAddTag,
                navigateNote = navigateNote
            )
        }
    }
    AddTagDialog(
        uiState = uiState,
        isShow = uiState.isShowAddTag,
        changeVisible = changeShowAddTag,
        addTag = addTag
    )
}

@ExperimentalPagerApi
@Composable
fun TagItemList(
    tags: List<BasicTagEntity>,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    backParent: (BasicTagEntity) -> Unit,
    changeShowAddTag: (Long?,Boolean) -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
) {
    Column(modifier = Modifier
        .verticalScroll(rememberScrollState())
    ) {
        if(tags.isNotEmpty())
        tags.forEachIndexed { index, tag ->
            BaseTagItem(
                tagEntity = tag,
                selectTag = selectTag,
                backParent = backParent,
                changeShowAddTag = changeShowAddTag,
                navigateNote = navigateNote
            )
            Spacer(modifier = Modifier.height(10.dp))
        }
    }
}

@ExperimentalPagerApi
@Composable
fun BaseTagItem(
    tagEntity: BasicTagEntity,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    backParent: (BasicTagEntity) -> Unit,
    changeShowAddTag: (Long?,Boolean) -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxWidth()
    ){
        Column{
            BaseItemTop(
                tagEntity = tagEntity,
                backParent = backParent
            )

            BaseItemBody(
                tagEntity = tagEntity,
                selectTag = selectTag,
                changeShowAddTag = changeShowAddTag,
                navigateNote = navigateNote
            )

            BaseItemBottom(tagEntity = tagEntity)

        }
    }
}

@Composable
fun NotePagerItem(
    position: Int,
    tagEntity: BasicTagEntity,
    navigateNote: (NoteEntity?) -> Unit,
    modifier: Modifier
) {
    Surface(
        shape = RoundedCornerShape(3.dp),
        modifier = modifier.click {
            navigateNote(tagEntity.notesList[position])
        }
    ) {
        Column(modifier = Modifier.padding(2.dp)) {
            Text(
                text = tagEntity.notesList[position].title,
                fontSize = 18.sp
            )
            Text(
                text = tagEntity.notesList[position].content,
                maxLines = 4,
                overflow = TextOverflow.Ellipsis,
                fontSize = 12.sp,
            )
            Text(
                text = longToDateFormat(tagEntity.notesList[position].time.time,"yyyy年MM月dd日"),
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                color = ModeTextColor()
            )
        }
    }
}

@Composable
fun BaseItemTop(
    tagEntity: BasicTagEntity,
    backParent: (BasicTagEntity) -> Unit
){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp, top = 10.dp, bottom = 10.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxHeight()
        ) {
            Image(
                painter = painterResource(id = R.drawable.higher),
                contentDescription = "上一级",
                modifier = Modifier
                    .size(30.dp)
                    .clickable {
                        tagEntity.parent?.let {
                            log("父标签${it}")
                            backParent(it)
                        }
                    }
            )
            Text(
                text = "上一级",
                fontSize = 13.sp,
                fontStyle = FontStyle.Italic,
                color = Grey
            )
        }
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(
                text = tagEntity.topTag.tag.name,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
fun BaseItemBottom(
    tagEntity: BasicTagEntity,
){
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            buildAnnotatedString {
                append("该标签下有笔记")
                withStyle(SpanStyle(color = PrimaryColor_60)) {
                    append("${tagEntity.notesList.size}")
                }
                append("篇")
            }, color = Grey, fontSize = 13.sp
        )
    }
}

@ExperimentalPagerApi
@Composable
fun BaseItemBody(
    tagEntity: BasicTagEntity,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    changeShowAddTag: (Long?,Boolean) -> Unit,
    navigateNote: (NoteEntity?) -> Unit,
){
    Surface(
        modifier = Modifier
            .height(250.dp)
            .padding(10.dp),
        elevation = 8.dp,
        shape = RoundedCornerShape(15.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            FlowRow(
                mainAxisSpacing = 5.dp,
                crossAxisSpacing = 8.dp,
                modifier = Modifier
                    .padding(top = 10.dp)
                    .weight(1f)
            ) {
                tagEntity.children.forEach {
                    TagItem(
                        tag = it,
                        basicTag = tagEntity,
                        selectTag = selectTag
                    )
                }
                AddTagItem(
                    parentId = tagEntity.topTag.tag.tagId,
                    changeShowAddTag = changeShowAddTag
                )
            }
//            Box(modifier = Modifier.weight(1f)){
//                Box(modifier = Modifier.fillMaxSize(),contentAlignment = Alignment.BottomCenter,){
//                    Text(
//                        text = "+添加标签"
//                    )
//                }
//                FlowRow(
//                    mainAxisSpacing = 5.dp,
//                    crossAxisSpacing = 8.dp,
//                    modifier = Modifier
//                        .padding(top = 10.dp)
//                ) {
//                    tagEntity.children.forEach {
//                        TagItem(
//                            tag = it,
//                            basicTag = tagEntity,
//                            selectTag = selectTag
//                        )
//                    }
//                }
//            }
            Box(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight(),
                contentAlignment = Alignment.Center
            ) {
                if (tagEntity.notesList.isNotEmpty()) {
                    HasNotePager(
                        tagEntity = tagEntity,
                        navigateNote = navigateNote)
                } else {
                    EmptyNotePage()
                }
            }
        }
    }
}

@Composable
fun EmptyNotePage() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "该标签下暂时没有笔记哦~",
            fontSize = 13.sp,
            color = Grey_60
        )
    }
}

@ExperimentalPagerApi
@Composable
fun HasNotePager(
    tagEntity: BasicTagEntity,
    navigateNote: (NoteEntity?) -> Unit,
) {
    val pagerState = rememberPagerState()
    Column(Modifier.fillMaxSize()) {
        HorizontalPager(
            count = tagEntity.notesList.size,
            state = pagerState,
        ) {page->
            NotePagerItem(
                position = page,
                tagEntity = tagEntity,
                navigateNote = navigateNote,
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f))
        }
        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .padding(16.dp)
                .align(Alignment.CenterHorizontally),
            activeColor = PrimaryColor,
            inactiveColor = PrimaryColor_35,
            indicatorWidth = 5.dp,
            indicatorHeight = 5.dp
        )
    }
}


@Composable
fun TagItem(
    basicTag: BasicTagEntity,
    selectTag: (TagWithNote, BasicTagEntity) -> Unit,
    tag: TagWithNote
) {
    val color = remember { randomColor() }
    val textColor = remember {
        if (color.isLight) TextColor_Black_60 else TextWhiteColor
    }
    Surface(
        color = color.color,
        shape = RoundedCornerShape(5.dp),
        elevation = 5.dp,
        modifier = Modifier
            .defaultMinSize(minHeight = 20.dp, minWidth = 40.dp)
            .clickable {
                selectTag(tag, basicTag)
            },
    ) {
        Text(
            text = tag.tag.name,
            color = textColor,
            fontSize = 13.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 4.dp, bottom = 4.dp, start = 5.dp, end = 5.dp)
        )
    }
}

@Composable
fun AddTagItem(
    parentId: Long? = null,
    changeShowAddTag: (Long?,Boolean) -> Unit
){
    Surface(
        color = ModeBgColor(),
        shape = RoundedCornerShape(5.dp),
        elevation = 5.dp,
        modifier = Modifier
            .defaultMinSize(minHeight = 20.dp, minWidth = 40.dp)
            .clickable {
                changeShowAddTag(parentId, true)
            },
    ) {
        Text(
            text = "+添加标签",
            color = PrimaryColor,
            fontSize = 13.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(top = 4.dp, bottom = 4.dp, start = 5.dp, end = 5.dp)
        )
    }

}

@Composable
fun AddTagDialog(
    uiState: TagsUiState,
    isShow: Boolean,
    changeVisible: (Long?,Boolean) -> Unit,
    addTag:(TagEntity) -> Unit
){
    var text by remember{ mutableStateOf("") }
    AnimatedVisibility(
        visible = isShow,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Box(modifier = Modifier
            .background(color = Black_60)
            .fillMaxSize(),
            contentAlignment = Alignment.Center
        ){
            Surface(
                shape = RoundedCornerShape(15.dp),
                modifier = Modifier
                    .width(200.dp)
                    .height(130.dp),
            ) {
                Column() {
                    TextField(
                        value = text,
                        onValueChange = {
                            text = it
                        },colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = GainColor,
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent
                        ),
                        shape = RoundedCornerShape(10.dp),
                        label = {Text("标签",color = PrimaryColor,fontSize = 13.sp,modifier = Modifier.alpha(0.6f))},
                        modifier = Modifier.padding(top = 5.dp,start = 10.dp,end = 10.dp,bottom = 5.dp)
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Row{
                        Text(text = "取消",
                            textAlign = TextAlign.Center,
                            color = PrimaryColor,
                            modifier = Modifier
                                .weight(1f)
                                .click { changeVisible(null, false) }
                        )
                        Text(text = "添加",
                            textAlign = TextAlign.Center,
                            color = PrimaryColor,
                            modifier = Modifier
                                .weight(1f)
                                .click {
                                    if (isShow) {
                                        val tagEntity = TagEntity(
                                            name = text,
                                            parentId = uiState.selectParentId
                                        )
                                        addTag(tagEntity)
                                        changeVisible(null, false)
                                    }
                                }
                        )
                    }
                }
            }
        }
        BackHandler() {
            changeVisible(null,false)
        }
    }

}
