package com.example.composeproject.ui.notedetail

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Editable
import android.text.Html
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.composeproject.entity.*
import com.example.composeproject.repository.NoteRepository
import com.example.composeproject.util.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*

data class NoteUiState(
    val bitmap: Bitmap? = null,
    val note: NoteEntity = NoteEntity(),
    val tags: List<TagEntity> = listOf(),
    val currentCursor: Int = 0,
    val isAddImage: Boolean = false,
    val noteTextList: List<NoteTextEntity> = listOf(),
    val isSave: Boolean = false,
    val isInit: Boolean = false,
    val isAddNewNote: Boolean = false,
    val isDeleteTag: Boolean = false,
    val isAddTag: Boolean = false
)

class NoteDetailViewModel(private val repository: NoteRepository,noteId: Long): ViewModel() {

    private val _uiState = MutableStateFlow(NoteUiState())
    val uiState = _uiState.asStateFlow()

    val noteState = noteStateAsMutableState()

    init {
        if (noteId != 0L) {
            getNoteContent(noteId)
        }else{
            openNewNote()
        }
    }

    private fun getNoteContent(id: Long){
        viewModelScope.launch {
            val noteWithTag = repository.getData(id = id)
            val note = noteWithTag.note
            val tagList = noteWithTag.tagList
            val content = repository.readNote(note.path)
            _uiState.update {
                val list = XmlUtil.getString(content)
                list.forEach { xml ->
                    if(xml.type == ContentType.Image){
                        xml.bitmap = BitmapFactory.decodeFile(repository.pathToFilePath(xml.path))
                    }
                }
                it.copy(
                    noteTextList = list,
                    note = note,
                    isInit = true,
                    tags = tagList
                )
            }
        }
    }

    private fun openNewNote(){
        _uiState.update {
            it.copy(
                isAddImage = false,
                isSave = false,
                isAddNewNote = true
            )
        }
    }

    fun addImage(bitmap: Bitmap){
        _uiState.update {
            it.copy(
                bitmap = bitmap,
                isAddImage = true
            )
        }
    }

    fun notAddImage(){
        _uiState.update {
            it.copy(
                bitmap = null,
                isAddImage = false
            )
        }
    }

    fun updateTile(t: String){
        viewModelScope.launch {
            if(!_uiState.value.isAddNewNote)
                repository.updateTitle(note = _uiState.value.note)
            _uiState.update {
                it.copy(
                    note = it.note.apply { title = t}
                )
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun save(text: Editable,error: () -> Unit, success: () -> Unit){
        viewModelScope.launch {
            if(_uiState.value.isAddNewNote){
                val newContent = if(text.length >= 50) text.toString().substring(0..50) else text.toString()
                val currentTime = Calendar.getInstance().time
                val noteWithTag = NoteWithTag(
                    note = _uiState.value.note.apply {
                        content = newContent
                        time = currentTime
                        path = "${currentTime.time}"
                        userId = UserUtil.getUser()?.id?: 0                            },
                    tagList = _uiState.value.tags
                )
                insertNote(note = noteWithTag,text = text,error = error,success = success)
            }else{
                val newContent = if(text.length >= 50) text.toString().substring(0..50) else text.toString()
                val noteWithTag = NoteWithTag(
                    note = _uiState.value.note.apply { content = newContent },
                    tagList = _uiState.value.tags
                )
                updateNote(note = noteWithTag)
                saveNote(text = text,error = error,success = success)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private suspend fun saveNote(text: Editable, error: () -> Unit, success: () -> Unit){
        val content = Html.toHtml(text,Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)
        val newContent = "<note>$content</note>"
        val notes = XmlUtil.getString(newContent)
        val newNotes = notes.filter{it.type == ContentType.Image}
        val imgMap = mutableMapOf<String,Drawable>()
        val drawables = getDrawables(text)
        drawables.forEachIndexed { index,drawable ->
            imgMap[newNotes[index].path] = drawable
        }
        repository.addOrUpdateNoteFile(content = newContent,name = _uiState.value.note.path)
        imgMap.forEach {
            repository.addImg(it.key,it.value)
        }
        _uiState.update {
            it.copy(
                isSave = false
            )
        }
        success()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private suspend fun insertNote(note: NoteWithTag, text: Editable, error: () -> Unit, success: () -> Unit){
        repository.addNote(note = note)
        saveNote(text = text,error = error,success = success)
    }

    private suspend fun updateNote(note: NoteWithTag){
        repository.updateNoteContent(note = note)
    }

    fun addTag(tag: TagEntity){
        _uiState.update {
            val tags = mutableListOf<TagEntity>()
            tags.addAll(it.tags)
            tags.add(tag)
            it.copy(
                tags = tags
            )
        }
    }

    fun deleteTag(tag: TagEntity){
        _uiState.update {
            val tags = mutableListOf<TagEntity>()
            tags.addAll(it.tags)
            tags.remove(tag)
            it.copy(
                tags = tags
            )
        }
    }

    fun updateSaveState(){
        _uiState.update {
            it.copy(
                isSave = true
            )
        }
    }

    companion object {
        fun provideFactory(
            postsRepository: NoteRepository,
            id: Long
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return NoteDetailViewModel(postsRepository,id) as T
            }
        }
    }
}