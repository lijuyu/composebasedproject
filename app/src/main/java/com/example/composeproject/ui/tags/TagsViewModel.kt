package com.example.composeproject.ui.tags

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.composeproject.entity.BasicTagEntity
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import com.example.composeproject.repository.TagsRepository
import com.example.composeproject.util.log
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


data class TagsUiState(
    val tagList: List<BasicTagEntity> = emptyList(),
    val isShowAddTag: Boolean = false,
    val selectParentId: Long? = null
)

class TagsViewModel(private val repository: TagsRepository) : ViewModel() {

    private val _uiState = MutableStateFlow(TagsUiState())
    val uiState = _uiState.asStateFlow()

    init {
        getAllTag()
    }

    fun getAllTag() {
        viewModelScope.launch {
            val flow = repository.getAllTag()
            flow.collect { tags ->
                val map = tags.groupBy { it.tag.parentId } as MutableMap
                val tagsList = tags.filter { it.tag.parentId == null }
                val tags = mutableListOf<BasicTagEntity>()
                val list = mutableListOf<TagEntity?>()
                tagsList.forEachIndexed { index, tag ->
                    val list = mutableListOf<TagWithNote>()
                    map.keys.forEach { parentId ->
                        if (tag.tag.tagId == parentId) {
                            list.addAll(map[parentId]!!)
                        }
                    }
                    tags.add(
                        BasicTagEntity(
                            id = index,
                            parent = null,
                            topTag = tag,
                            children = list,
                            notesList = tag.noteList
                        )
                    )
                }
                _uiState.update {
                    repeat(tags.size) {
                        list.add(null)
                    }
                    it.copy(
                        tagList = tags
                    )
                }
            }
        }
    }

    private fun select(
        isBack: Boolean = false,
        tag: TagWithNote? = null,
        tagEntity: BasicTagEntity
    ) {
        viewModelScope.launch {
            val list = mutableListOf<BasicTagEntity>()
            list.addAll(_uiState.value.tagList)
            val tagFlow = repository.getChildTags(
                tag?.tag?.tagId ?: tagEntity.topTag.tag.tagId
            )
            val newTag = BasicTagEntity(
                id = tagEntity.id,
                parent = if(isBack)tagEntity.parent else tagEntity,
                topTag = tag ?: tagEntity.topTag,
            )
            tagFlow.collect {
                newTag.children = it
                val noteFlow =
                    repository.getNotesByTagId(tag?.tag?.tagId ?: tagEntity.topTag.tag.tagId)
                noteFlow.collect {
                    newTag.notesList = it.noteList
                    for (i in list.indices) {
                        if (list[i].id == newTag.id) {
                            list[i] = newTag
                        }
                    }
                    _uiState.update {
                        it.copy(
                            tagList = list
                        )
                    }
                }
            }
        }
    }

    fun selectTag(tag: TagWithNote, tagEntity: BasicTagEntity) {
        select(tag = tag, tagEntity = tagEntity)
    }

    fun backParent(tagEntity: BasicTagEntity) {
        select(isBack = true,tagEntity = tagEntity)
    }

    fun addChildTag(tag: TagEntity){
        viewModelScope.launch {
            repository.addChildTag(tag = tag)
        }
    }

    fun changeShowAddTag(selectParentId: Long?,isShow: Boolean){
        _uiState.update {
            it.copy(
                isShowAddTag = isShow,
                selectParentId = selectParentId
            )
        }
    }

    companion object {
        fun provideFactory(
            postsRepository: TagsRepository,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return TagsViewModel(postsRepository) as T
            }
        }
    }

}