package com.example.composeproject.ui.home.state

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.*
import com.example.composeproject.ui.component.ImageState
import kotlinx.coroutines.CoroutineScope

@ExperimentalMaterialApi
data class BaseHomeState(
    val backDropScaffoldState: BackdropScaffoldState,
    val scope: CoroutineScope,
    val imageState : ImageState,
    val homeListLazyState: LazyListState,
    val startTime: Long
)

@ExperimentalMaterialApi
@Composable
fun rememberHomeState(
    backDropScaffoldState: BackdropScaffoldState = rememberBackdropScaffoldState(initialValue = BackdropValue.Concealed),
    scope: CoroutineScope = rememberCoroutineScope(),
    imageState: ImageState = remember { ImageState()},
    homeListLazyState: LazyListState = rememberLazyListState(),
    startTime: Long = 0L
): BaseHomeState = remember(backDropScaffoldState,scope,imageState) {
    mutableStateOf(BaseHomeState(backDropScaffoldState,scope,imageState,homeListLazyState,startTime))
}.value