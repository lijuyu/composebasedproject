package com.example.composeproject.util

import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.os.Binder
import android.view.WindowManager
import androidx.compose.ui.graphics.Color
import com.example.composeproject.ui.base.NoteApplication
import com.google.android.material.internal.ContextUtils


inline fun <reified T>Context.launch(start: Intent.() -> Unit = {}){
    val intent = Intent(this,T::class.java)
    start(intent)
    this.startActivity(intent)
}

fun Context.screenWidth(): Int{
    val p = Point()
    (this.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getSize(p)
    return p.x
}

fun Context.screenHeight(): Int{
    val p = Point()
    (this.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getSize(p)
    return p.y
}

fun Context.isDark(): Boolean{
    val application = this.applicationContext as NoteApplication
    return application.getSharedPrefEditInstance().getBoolean(DARK_MODE,false)
}

fun Context.getNoteBg(): Int{
    val application = this.applicationContext as NoteApplication
    return application.getSharedPrefEditInstance().getInt(NOTE_BACKGROUND,0)
}

fun Context.setNoteBg(bg: Int){
    val application = this.applicationContext as NoteApplication
    application.getSharedPrefEditInstance().edit().apply {
        putInt(NOTE_BACKGROUND,bg)
        apply()
    }
}

//fun Context.setPrimaryColor(color: String) {
//    val application = this.applicationContext as NoteApplication
//    application.getSharedPrefEditInstance().edit().apply {
//        putInt(PRIMARY_COLOR,)
//        apply()
//    }
//}
//通过反射判断悬浮窗权限
fun Context.checkOverlayPermission(): Boolean{
    val op = 24
    val manager = this.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
    val c = AppOpsManager::class.java
    kotlin.runCatching {
        val method = c.getDeclaredMethod("checkOp",Int::class.java,Int::class.java,String::class.java)
        return AppOpsManager.MODE_ALLOWED == method.invoke(manager,op,Binder.getCallingUid(),this.packageName)
    }
    return false
}