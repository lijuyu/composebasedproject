package com.example.composeproject.util

import com.squareup.moshi.Moshi

class MoshiUtil {

    companion object{
        inline fun <reified T>toJson(text: T): String{
            val moshi = Moshi.Builder().build()
            val adapter = moshi.adapter(T::class.java)
            return adapter.toJson(text)
        }

        inline fun <reified T>fromJson(text: String): T?{
            val moshi = Moshi.Builder().build()
            val adapter = moshi.adapter(T::class.java)
            return adapter.fromJson(text)
        }
    }
}