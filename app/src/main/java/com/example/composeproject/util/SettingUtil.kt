package com.example.composeproject.util

const val HAS_INIT = "init"
const val SETTING_SHARED_PREF = "setting"
const val OVERLAY_VALUE = "overlay"
const val DARK_MODE= "dark"
const val NOTE_BACKGROUND = "note_bg"
const val PRIMARY_COLOR = "primary_color"

val settingArrays =
    listOf(
        "悬浮窗",
        "深夜模式"
    )