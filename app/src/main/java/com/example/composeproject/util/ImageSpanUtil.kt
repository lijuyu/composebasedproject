package com.example.composeproject.util

import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan


fun getDrawables(span: Editable): List<Drawable>{
    val list = mutableListOf<Drawable>()
    val s = span.getSpans(0,span.length,ImageSpan::class.java)
    s.forEach {
        list.add(it.drawable)
    }
    return list
}