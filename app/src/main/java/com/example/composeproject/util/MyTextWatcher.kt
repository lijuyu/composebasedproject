package com.example.composeproject.util

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.text.*
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.text.style.ImageSpan
import android.text.style.StyleSpan
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.core.text.toHtml
import com.example.composeproject.entity.FontColor
import com.example.composeproject.ui.notedetail.TextSize
import java.util.*

@RequiresApi(Build.VERSION_CODES.N)
class MyTextWatcher(
    private val changeAddImage: () -> Unit
): TextWatcher {

    private var selectAfterText = ""
    private var lastText = ""
    private var select = 0
    var textColor: FontColor = FontColor(value = "#000000",color = androidx.compose.ui.graphics.Color.Black)
    var isBold :Boolean = false
    var isItalic: Boolean = false
    var fontSize: TextSize = TextSize()
    lateinit var editText: EditText
    var isAddImage = false
    var bitmap: Bitmap? = null

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        with(editText){
            select = selectionStart
            selectAfterText = p0?.substring(0,selectionStart).toString()
            lastText = p0.toString()
        }
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        with(editText){
            removeTextChangedListener(this@MyTextWatcher)
            if(lastText.length < text.length){
                val s = p0?.substring(select, p0.length - lastText.length + select).toString()
                val span = setTextSpan(s,color = textColor,fontSize = fontSize,isItalic = isItalic,isBold = isBold)
                text.replace(selectAfterText.lastIndex+1,selectAfterText.length + s.length,span)
                setSelection(p0!!.length - lastText.length + select)
            }
            addTextChangedListener(this@MyTextWatcher)
        }
    }

    override fun afterTextChanged(p0: Editable?) {

    }

    @SuppressLint("Range")
    private fun setTextSpan(text: String, color: FontColor, fontSize: TextSize, isBold: Boolean, isItalic: Boolean): SpannableStringBuilder {
        val spannable = SpannableStringBuilder(text)
        if(isAddImage){
            bitmap?.let {
                val time = Calendar.getInstance().time.time
                val s = Html.fromHtml("<img src=${time}></img>",Html.FROM_HTML_MODE_COMPACT,ImageGetter(editText.context,it),null)
                val i = s.getSpans(0,s.length,ImageSpan::class.java)
                spannable.setSpan(i[0],0,text.length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                isAddImage = false
                bitmap = null
                changeAddImage()
            }
        }else{
            spannable.setSpan(ForegroundColorSpan(Color.parseColor(color.value)),0,text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            if(isBold){
                spannable.setSpan(StyleSpan(Typeface.BOLD),0,text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            if(isItalic){
                spannable.setSpan(StyleSpan(Typeface.ITALIC),0,text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            spannable.setSpan(AbsoluteSizeSpan(fontSize.value),0,text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        return spannable
    }

}