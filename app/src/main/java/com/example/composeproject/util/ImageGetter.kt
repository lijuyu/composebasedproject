package com.example.composeproject.util

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.Shape
import android.text.Editable
import android.text.Html
import org.xml.sax.XMLReader
import java.io.File

class ImageGetter(val context: Context, val bitmap: Bitmap) : Html.ImageGetter {

    companion object {
        const val BITMAP_ROUNDER = 20f
    }

    override fun getDrawable(p0: String?): Drawable {
        return BitmapDrawable(context.resources, bitmap.bitmapRound(context = context)).apply {
            setBounds(0, 0, this.intrinsicWidth, this.intrinsicHeight)
        }
    }
}

fun Bitmap.bitmapRound(context: Context): Bitmap {

    val newWidth = if (this.width > context.screenWidth())
        (context.screenWidth().toFloat() / this.width.toFloat()) * this.width
    else
        (this.width.toFloat() / context.screenWidth().toFloat()) * this.width

    val newHeight = if (this.height > context.screenHeight())
        (context.screenHeight().toFloat() / this.height.toFloat()) * this.height
    else
        (this.height.toFloat() / context.screenHeight().toFloat()) * this.height

    val bitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    val paint = Paint().apply {
        isAntiAlias = true
    }
    val rect = Rect(0, 0, canvas.width, canvas.height)
    val rectF = RectF(rect)
    canvas.drawARGB(0, 0, 0, 0)
    canvas.drawRoundRect(rectF, ImageGetter.BITMAP_ROUNDER, ImageGetter.BITMAP_ROUNDER, paint)
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(this, rect, rect, paint)
    return bitmap
}