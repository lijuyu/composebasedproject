package com.example.composeproject.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Environment
import androidx.core.graphics.drawable.toBitmap
import kotlinx.coroutines.runBlocking
import java.io.*

//suspend fun updateContent(context: Context, name: String, content: String) {
//    val file = File(context.getExternalFilesDir(null), "$name.txt")
//    if (!file.exists()) {
//        file.createNewFile()
//    }
//    val fileWriter = FileWriter(file, true)
//    fileWriter.write(content)
//    fileWriter.flush()
//    fileWriter.close()
//}

suspend fun deleteNoteFile(context: Context, name: String) {
    val file = File(context.getExternalFilesDir(null), "$name.txt")
    file.delete()
}

suspend fun addOrUpdateNoteFile(context: Context, name: String, content: String) : Boolean{
    val file = File(context.getExternalFilesDir(null), "$name.txt")
    if (file.exists()){
        file.delete()
    }
    file.createNewFile()
    runCatching {
        val fileWriter = FileWriter(file, true)
        with(fileWriter) {
            write(content)
            flush()
            close()
        }
        return true
    }
    return false
}

suspend fun addImage(context: Context, name: String, bitmap: Bitmap): Bitmap {
    val file = File(context.getExternalFilesDir(null), "$name.png")
    if (!file.exists()) {
        file.createNewFile()
    }
    val out = file.outputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
    val newBit = BitmapFactory.decodeFile(file.path)
    out.flush()
    out.close()
    return newBit
}

suspend fun deleteImage(context: Context, name: String) {
    val file = File(context.getExternalFilesDir(null), "$name.png")
    file.delete()
}

suspend fun readNoteFile(context: Context, name: String): String {
    val file = File(context.getExternalFilesDir(null), "$name.txt")
    if (!file.exists()) {
        file.createNewFile()
    }
    val fileIns = FileInputStream(file)
    val b = ByteArray(1024)
    var len = 0
    val baos = ByteArrayOutputStream()
    while (fileIns.read(b).also { len = it } != -1) {
        baos.write(b, 0, len)
    }
    val data = baos.toByteArray()
    baos.close()
    fileIns.close()
    return String(data)
}

suspend fun drawableToFile(context: Context,drawable: Drawable,name: String): String{
    val bitmap = drawable.toBitmap()
    val file = File(context.getExternalFilesDir(null), "$name.png")
    if (!file.exists()) {
        file.createNewFile()
    }
    val out = file.outputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
    out.flush()
    out.close()
    return file.path
}