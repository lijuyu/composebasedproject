package com.example.composeproject.util

import com.example.composeproject.dao.UserDao
import com.example.composeproject.entity.UserEntity

object UserUtil {

    private var user: UserEntity? = null
    var dao: UserDao? = null

    suspend fun loadUser(){
        user = dao?.getUser()
    }

    @JvmName("getUser1")
    fun getUser(): UserEntity?{
        return user
    }
}