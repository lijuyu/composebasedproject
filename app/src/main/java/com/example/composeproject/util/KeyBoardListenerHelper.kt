package com.example.composeproject.util

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewTreeObserver
import java.lang.ref.WeakReference

class KeyBoardListenerHelper(var activity: Activity) {

    private val weakActivity: WeakReference<Activity>? by lazy { WeakReference(activity) }
    private var listener: OnKeyBoardChangeListener? = null
    private val onGlobalLayoutListener:ViewTreeObserver.OnGlobalLayoutListener by lazy {
        ViewTreeObserver.OnGlobalLayoutListener {
            weakActivity?.let {
                val rect = Rect()
                var screenHeight = 0
                var keyBoardHeight = 0
                it.get()?.let{  a ->
                    a.window.decorView.getWindowVisibleDisplayFrame(rect)
                    screenHeight = a.window.decorView.height
                    keyBoardHeight = screenHeight - rect.bottom
                    listener?.let { l ->
                        l.onKeyBoardChange(keyBoardHeight > 0,keyBoardHeight)
                    }
                }
            }
        }
    }

    init {
        val content: View = activity.findViewById(android.R.id.content)
        content.viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
    }

    fun setOnKeyBoardChangeListener(onKeyBoardChangeListener: OnKeyBoardChangeListener){
        listener = onKeyBoardChangeListener
    }

    fun destroy(){
        val content: View = activity.findViewById(android.R.id.content)
        content.viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
    }

}

interface OnKeyBoardChangeListener{
    open fun onKeyBoardChange(isShow: Boolean,keyBoardHeight: Int)
}