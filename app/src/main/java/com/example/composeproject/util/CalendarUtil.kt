package com.example.composeproject.util

import android.util.Log
import com.example.composeproject.entity.DayEntity
import com.example.composeproject.entity.MonthEntity
import java.text.SimpleDateFormat
import java.util.*

fun monthInEnglish(month: Int): String {
    return when (month) {
        1 -> "Jan."
        2 -> "Feb."
        3 -> "Mar."
        4 -> "Apr."
        5 -> "May."
        6 -> "Jun."
        7 -> "Jul."
        8 -> "Aug."
        9 -> "Sep."
        10 -> "Oct."
        11 -> "Nov."
        12 -> "Dec."
        else -> "error"
    }
}

fun getCurrentMonth(): Int{
    return Calendar.getInstance().get(Calendar.MONTH) + 1
}

fun getCurrentDay(): Int{
    return Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
}

fun getDayIntCurrentMonth(month: Int): List<DayEntity>{
    val list = mutableListOf<DayEntity>()
    val calendar = Calendar.getInstance().apply {
        set(Calendar.MONTH,month - 1)
        set(Calendar.DATE,1)
        roll(Calendar.DATE,-1)
    }
    val first = 7 - getFirstDayOfWeek(month) + 1
    val firstLine = 7 - first
    val days = calendar.getActualMaximum(Calendar.DATE)
    val lastLine = 7 - getLastDayOfWeek(month)
    var day = 1
    val currentDay = getCurrentDay()
    for(i in 1 .. days + firstLine + lastLine){
        val timeEntity = DayEntity(month = month)
        if(day == currentDay){
            Log.d("当前:","$day 和 $currentDay")
            timeEntity.isSelect = true
        }
        if(i <= firstLine){

        }else if(i > firstLine + days){

        }else{
            timeEntity.day = day
            day++
        }
        list.add(timeEntity)
    }
    return list
}

fun weekInterToString(week: Int): String{
    return when(week){
        1 -> "一"
        2 -> "二"
        3 -> "三"
        4 -> "四"
        5 -> "五"
        6 -> "六"
       else -> "日"
    }
}

fun getFirstDayOfWeek(month: Int): Int{
    val calendar = Calendar.getInstance().apply {
        set(Calendar.MONTH,month - 1)
        set(Calendar.DATE,1)
    }
    val week = calendar.get(Calendar.DAY_OF_WEEK)
    return week
}

fun getLastDayOfWeek(month: Int): Int{
    val calendar = Calendar.getInstance().apply {
        set(Calendar.MONTH,month - 1)
        set(Calendar.DATE,1)
        roll(Calendar.DATE,-1)
    }
    val week = calendar.get(Calendar.DAY_OF_WEEK)
    return week
}

fun longToDateFormat(time: Long,format: String): String{
    val sdf = SimpleDateFormat(format)
    val date = Date(time)
    val str = sdf.format(date)
    return str
}

fun dateToLong(format: String,time: String): Long{
    val sdf = SimpleDateFormat(format)
    val d = sdf.parse(format)
    return d.time
}
