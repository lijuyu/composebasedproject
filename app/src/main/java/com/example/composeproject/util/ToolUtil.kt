package com.example.composeproject.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composeproject.R

@Composable
fun toolMap(): Map<ClickEnum, Painter> {
    return mutableMapOf(
        ClickEnum.Image to painterResource(id = R.drawable.img),
        ClickEnum.Background to painterResource(id = R.drawable.bg),
        ClickEnum.Font to painterResource(id = R.drawable.font),
    )
}

val fontSizes = mapOf<Dp,TextUnit>(
    15.dp to 15.sp,
    20.dp to 20.sp,
    30.dp to 30.sp,
    40.dp to 40.sp
)

enum class ClickEnum(val value: String){
    Image("图片"),Font("字体"),Background("背景")
}

enum class SpecialBgEnum(val value: String){
    Grid("grid"),
    StripeHor("stripeHor"),
    StripeVer("stripeVer"),
    Special("special")
}

val homeToolArray =
    mapOf(
        "标签组" to R.drawable.tags,
        "设置" to R.drawable.setting
    )

val colorBg = mapOf(
    Color(0xFFEF9697) to R.drawable.fuzzy_red,
    Color(0xFF2E94CB) to R.drawable.fuzzy_blue,
    Color(0xFFAFA36E) to R.drawable.fuzzy_grown,
    Color(0xFF666EB9) to R.drawable.fuzzy_purple,
    Color(0xFFCBA1C7) to R.drawable.fuzzy_pink
)

@Composable
fun specialBg() = mapOf(
    SpecialBgEnum.Grid to R.drawable.grid_bg,
    SpecialBgEnum.StripeHor to R.drawable.stripe_hor,
    SpecialBgEnum.StripeVer to R.drawable.stripe_ver,
    SpecialBgEnum.Special to R.drawable.special
)