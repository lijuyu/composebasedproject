package com.example.composeproject.util

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextPaint
import android.text.style.ReplacementSpan

class MyNoteSpan(private val textColor: String,private val size: Float) : ReplacementSpan() {

    private val mPaint = TextPaint()
    private var mWidth = 0
    private var mHeight = 0

    init {
        initPaint()
    }


    override fun getSize(
        paint: Paint,
        text: CharSequence?,
        start: Int,
        end: Int,
        fm: Paint.FontMetricsInt?
    ): Int {
        return initSize(text.toString())
    }

    override fun draw(
        canvas: Canvas,
        text: CharSequence?,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        canvas.drawText(text.toString(),mWidth.toFloat() / 2,mHeight.toFloat() / 2,mPaint)
    }

    private fun initPaint(){
        mPaint.apply {
            color = Color.parseColor(textColor)
            textSize = size
            isAntiAlias = true
            textAlign = Paint.Align.CENTER
        }
    }

    private fun initSize(text: String): Int{
        val textPaint = Paint().apply {
            textSize = size
        }
        val textRect = Rect()
        textPaint.getTextBounds(text,0,text.length,textRect)
        mWidth = textRect.width()
        mHeight = textRect.height()
        return textRect.width()
    }


}