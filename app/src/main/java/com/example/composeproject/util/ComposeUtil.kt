package com.example.composeproject.util

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import com.example.composeproject.R
import com.example.composeproject.ui.base.NoteApplication
import com.example.composeproject.ui.theme.DarkPrimaryColor
import com.example.composeproject.ui.theme.Grey_60
import com.example.composeproject.ui.theme.SwitchPrimaryColor

@Composable
fun ModeColor(): Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.White
    }else{
        DarkPrimaryColor
    }
}

@Composable
fun ModeBgColor(): Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        DarkPrimaryColor
    }else{
        Color.White
    }
}

@Composable
fun ModeTextColor() :Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.White
    }else{
        Color.Black
    }
}

@Composable
fun ModeBgColorGrey(): Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.Black
    }else{
        Grey_60
    }
}

@Composable
fun SwitchCheckedThumbColor() : Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.White
    }else{
        SwitchPrimaryColor
    }
}

@Composable
fun SwitchCheckedTrackColor() : Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.Black
    }else{
        SwitchPrimaryColor
    }
}

@Composable
fun SwitchUnCheckedThumbColor() : Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.White
    }else{
        Color.White
    }
}

@Composable
fun SwitchUnCheckedTrackColor() : Color{
    val application = LocalContext.current.applicationContext as NoteApplication
    return if(application.isDark()){
        Color.Black
    }else{
        Color.Black
    }
}

@Composable
fun AddTagImg(modifier: Modifier = Modifier){
    val application = LocalContext.current.applicationContext as NoteApplication
    if(application.isDark()){
        Image(
            painter = painterResource(id = R.drawable.add_tag_light),
            contentDescription = null,
            modifier = modifier
        )
    }else{
        Image(
            painter = painterResource(id = R.drawable.add_tag_night),
            contentDescription = null,
            modifier = modifier
        )
    }
}

@Composable
fun GradientHorToRight(modifier: Modifier = Modifier){
    val color = ModeBgColor()
    Canvas(modifier = modifier){
        val deWidth = size.width / 200
        val deAlpha = 1.0f / 200
        for(i in 150 downTo 0){
            drawRect(
                color = color,
                size = Size(width = deWidth,height = size.height),
                alpha = deAlpha * i,
                topLeft = Offset(deWidth * (200 - i),0f)
            )
        }
    }
}

@Composable
fun GradientHorToLeft(modifier: Modifier = Modifier){
    val color = ModeBgColor()
    Canvas(modifier = modifier){
        val deWidth = size.width / 200
        val deAlpha = 1.0f / 200
        for(i in 0..200){
            drawRect(
                color = color,
                size = Size(width = deWidth,height = size.height),
                alpha = deAlpha * i,
                topLeft = Offset(deWidth * i,0f)
            )
        }
    }
}

@Composable
fun TiTleEdit(modifier : Modifier = Modifier){
    val application = LocalContext.current.applicationContext as NoteApplication
    if(application.isDark()){
        Image(painter = painterResource(id = R.drawable.edit_light),null,modifier = modifier)
    }else{
        Image(painter = painterResource(id = R.drawable.edit_night),null,modifier = modifier)
    }
}

@Composable
fun TiTleEditSure(modifier : Modifier = Modifier){
    val application = LocalContext.current.applicationContext as NoteApplication
    if(application.isDark()){
        Image(painter = painterResource(id = R.drawable.sure_light),null,modifier = modifier)
    }else{
        Image(painter = painterResource(id = R.drawable.sure_night),null,modifier = modifier)
    }
}