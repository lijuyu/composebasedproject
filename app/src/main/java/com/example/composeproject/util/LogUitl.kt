package com.example.composeproject.util

import android.util.Log

fun log(s: String){
    Log.d("composeproject-log","$s")
}