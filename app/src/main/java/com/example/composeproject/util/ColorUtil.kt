package com.example.composeproject.util


import androidx.compose.ui.graphics.Color
import com.example.composeproject.entity.ColorEntity
import com.example.composeproject.entity.FontColor
import com.example.composeproject.ui.theme.*

fun randomColor(): ColorEntity{
    val colorArray = listOf(
        ColorEntity(color = PrimaryColor),
        ColorEntity(color = PrimaryColor_60),
        ColorEntity(color = LightSeaGreen),
        ColorEntity(color = Khaki,isLight = true),
        ColorEntity(color = Azure,isLight = true))
    val i = (0..4).random()
    return colorArray[i]
}

fun randomBorder():ColorEntity{
    val colorArray = listOf(
        ColorEntity(color = PrimaryColor),
        ColorEntity(color = PrimaryColor_60),
        ColorEntity(color = LightSeaGreen),
        ColorEntity(color = Khaki,isLight = true),
//        ColorEntity(color = Azure,isLight = true)
    )
    val i = (0..3).random()
    return colorArray[i]
}

val textColor = listOf(
    FontColor(value = "#000000",color = Color.Black),
    FontColor(value = "#4682B4",color = PrimaryColor),
    FontColor(value = "#0000FF",color = Color.Blue),
    FontColor(value = "#00FF00",color = Color.Green),
    FontColor(value = "#FFFF00",color = Color.Yellow),
    FontColor(value = "#FF0000",color = Color.Red),
    FontColor(value = "#FF00FF",color = Color.Magenta),
)