package com.example.composeproject.util

import com.example.composeproject.entity.ContentType
import com.example.composeproject.entity.FontColor
import com.example.composeproject.entity.NoteTextEntity
import com.example.composeproject.entity.TextWithSize
import com.example.composeproject.ui.notedetail.TextSize
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.StringReader

data class BoldWithItalic(
    var isBold: Boolean = false,
    var isItalic: Boolean = false
)

object XmlUtil {

    private val ns: String? = null

    fun getString(string: String): List<NoteTextEntity>{
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        val notes = mutableListOf<NoteTextEntity>()
        if(string.isNotEmpty()){
            val newString = string
                .replace("<span style=\"font-size:","<font size=\"")
                .replace("</span></span>","</font></span>")
                .replace(";>",">")
                .replace("\"></p>","\"></img></p>")
                .replace("\"><span","\"></img><span")
                .replace(";\"></img>",";\">")
                .replace("<br>","<dd></dd>")
                .replace("\n","<dd></dd>")
//                .replace("</p><p","</p><dd></dd><p")
            parser.setInput(StringReader(newString))
            parser.nextTag()
            return readNote(parser = parser)
        }else {
            return notes
        }
    }

    private fun readDD(parser: XmlPullParser): NoteTextEntity{
        val note = NoteTextEntity(text = "\n",fontSize = 0)
        parser.require(XmlPullParser.START_TAG,ns,"dd")
        while(parser.next() != XmlPullParser.END_TAG){
//            if(parser.eventType != XmlPullParser.START_TAG){
//                continue
//            }
        }
        return note
    }

    private fun readNote(parser: XmlPullParser):List<NoteTextEntity>{
        val notes = mutableListOf<NoteTextEntity>()
        parser.require(XmlPullParser.START_TAG,ns,"note")
        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.eventType != XmlPullParser.START_TAG){
                continue
            }
            if(parser.name == "p"){
                notes.addAll(readP(parser = parser))
            }else if(parser.name == "dd"){
                notes.add(readDD(parser = parser))
            }
        }
        return notes
    }

    private fun readP(parser: XmlPullParser): List<NoteTextEntity>{
        val notes = mutableListOf<NoteTextEntity>()
        parser.require(XmlPullParser.START_TAG, ns, "p")
        while (parser.next() != XmlPullParser.END_TAG){
            if(parser.eventType != XmlPullParser.START_TAG){
                continue
            }
            if(parser.name == "span"){
                notes.add(readSpan(parser = parser))
            }else if(parser.name == "img"){
                notes.add(readImg(parser = parser))
            }
        }
        return notes
    }

    private fun readSpan(parser: XmlPullParser): NoteTextEntity{
        val note = NoteTextEntity()
        var textSize = TextWithSize()
        val color = parser.getAttributeValue(null,"style")
            .replace("color:","")
            .replace(";","")
        var isItalic: Boolean? = null
        var boldWithItalic = BoldWithItalic()
        parser.require(XmlPullParser.START_TAG, ns, "span")
        while (parser.next() != XmlPullParser.END_TAG){
            if(parser.name == "font"){
                textSize = readFont(parser = parser)
            }
            if(parser.name == "b"){
                boldWithItalic = readB(parser = parser)
            }
            if(parser.name == "i"){
                isItalic = readI(parser = parser)
            }
        }
        note.text = textSize.text
        note.textColor = color
        note.fontSize = textSize.textSize
        note.isItalic = boldWithItalic.isItalic
        note.isBold = boldWithItalic.isBold
        note.type = ContentType.Text
        isItalic?.let {
            note.isItalic = it
        }
        return note
    }

    private fun readFont(parser: XmlPullParser): TextWithSize{
        val textSize = TextWithSize()
        textSize.textSize = parser.getAttributeValue(null,"size")
            .replace("px","")
            .toInt()
        parser.require(XmlPullParser.START_TAG, ns, "font")
        textSize.text = readText(parser = parser)
        parser.require(XmlPullParser.END_TAG, ns, "font")
        return textSize
    }

    private fun readImg(parser: XmlPullParser): NoteTextEntity{
        parser.require(XmlPullParser.START_TAG, ns, "img")
        val img = NoteTextEntity()
        val tag = parser.name
        if(tag == "img"){
            img.path = parser.getAttributeValue(null,"src")
            img.type = ContentType.Image
            parser.nextTag()
        }
        parser.require(XmlPullParser.END_TAG, ns, "img")
        return img
    }

    private fun readB(parser: XmlPullParser): BoldWithItalic{
        val boldWithItalic = BoldWithItalic(isBold = true)
        parser.require(XmlPullParser.START_TAG, ns, "b")
        while (parser.next()!= XmlPullParser.END_TAG){
            if(parser.name == "i"){
                boldWithItalic.isItalic = readI(parser = parser)
            }
        }
        return boldWithItalic
    }

    private fun readI(parser: XmlPullParser): Boolean{
        return true
    }

    private fun readText(parser: XmlPullParser): String{
        var text = ""
        if(parser.next() == XmlPullParser.TEXT){
            text = parser.text
            parser.nextTag()
        }
        return text
    }

}