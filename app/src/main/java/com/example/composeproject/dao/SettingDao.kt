package com.example.composeproject.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteTagCrossRef
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.UserEntity

@Dao
interface SettingDao {

    @Query("select * from note_table")
    suspend fun getNote(): List<NoteEntity>

    @Query("select * from user_table")
    suspend fun getUser(): UserEntity

    @Query("select * from tag_table")
    suspend fun getTag(): List<TagEntity>

    @Query("select * from note_tag_table")
    suspend fun getNoteTagCrossRef(): List<NoteTagCrossRef>
}