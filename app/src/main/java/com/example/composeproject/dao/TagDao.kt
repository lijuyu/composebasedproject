package com.example.composeproject.dao

import androidx.room.*
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.TagEntity
import com.example.composeproject.entity.TagWithNote
import kotlinx.coroutines.flow.Flow

@Dao
interface TagDao {

    @Transaction
    @Query("select * from tag_table")
    fun getAllTag(): Flow<List<TagWithNote>>

    @Transaction
    @Query("select * from tag_table where parentId = :parenId")
    fun getChildTags(parenId: Long): Flow<List<TagWithNote>>

    @Transaction
    @Query("select * from note_table where noteId = :noteId")
    fun getTagsByNoteId(noteId: Long): Flow<NoteWithTag>

    @Transaction
    @Query("select * from tag_table where tagId = :tagId")
    fun getNotesByTagId(tagId: Long): Flow<TagWithNote>

    @Insert
    suspend fun addChildTag(tag: TagEntity)

}