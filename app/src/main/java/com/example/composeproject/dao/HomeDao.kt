package com.example.composeproject.dao

import androidx.room.*
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.UserEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface HomeDao {

    @Transaction
    @Query("select * from note_table")
    fun getNoteList(): Flow<List<NoteWithTag>>

    @Query("select count(*) from tag_table")
    suspend fun getTagsNum(): Int

    @Query("select count(*) from note_table")
    suspend fun getNoteNum() :Int

    @Delete
    suspend fun deleteNotes(noteList: List<NoteEntity>)

    @Delete
    suspend fun deleteNote(note: NoteEntity)

}