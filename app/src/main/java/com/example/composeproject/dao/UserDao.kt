package com.example.composeproject.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.composeproject.entity.UserEntity

@Dao
interface UserDao {

    @Query("select * from user_table")
    fun getUser(): UserEntity

}