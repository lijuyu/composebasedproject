package com.example.composeproject.dao

import androidx.room.*
import com.example.composeproject.entity.NoteEntity
import com.example.composeproject.entity.NoteTagCrossRef
import com.example.composeproject.entity.NoteWithTag
import com.example.composeproject.entity.TagEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {

    @Insert
    suspend fun addNote(note: NoteEntity):Long

    @Update
    suspend fun updateTitle(note: NoteEntity)

    @Update
    suspend fun updateContent(note: NoteEntity)

    @Transaction
    @Query("select * from note_table where noteId = :id")
    suspend fun getData(id: Long): NoteWithTag

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTags(tags: List<TagEntity>):List<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addNoteWithTags(noteAndTags: List<NoteTagCrossRef>)

}